#!/usr/bin/env python

# This results in a login error html:
# curl -O https://redmine.biotech.ufl.edu/attachments/download/391/Run_Meerow_Zolotukhin_11-25-2014_340.Zolotukhin.Pacbio.ReadMe.txt
# so try pip install python-redmine module
#
# use virtualenv to setup python2 or python3 workspace (sudo yum install -y python-virtualenv or pip install virtualenv):
# virtualenv -p /opt/bin/python2.7 --clear --always-copy --prompt=py2env ./py2env ; . ./py2env/bin/activate
# virtualenv -p /opt/bin/python3.5 --clear --always-copy --prompt=py3env ./py3env ; . ./py3env/bin/activate

import os, sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

import inspect
from redmine import Redmine

def connect(url='https://redmine.biotech.ufl.edu', key='6c0c9e3c80f8ef1e317ba1075a62d01d5cc0ded0'):
  # return redmine object for future use
  icbred = Redmine(url, key)
  return icbred 
#end connect

def projInfo(projname='day-to-day', issueId=4686):
  """
  Redmine REST API to fetch or post data-delivery status and artifacts like the sha512 checksums.
  A fully QC'd data-delivery workflow:
  1.) Log onto Redmine and click into the "day-to-day activities" project.
  2.) Create a new Issue (task) for the data-delivery and make note of the Issue #.
  3.) Invoke the Python data-delivery script, which should prompt you for (or remind your to create it)
      the new issue/task #.
  4.) The Python script can then use the Redmine REST API to verify the new issue / task exists
      and proceed to gather info from the CSV or Excel file.
  5.) Once all file info. has been gathered, proceed to execute shasum -a 512 on each data file
      and concat checksum output to the log file.
  6.) Invoke shasum -c on the logfile to double-check the results.
  """
  funcname = inspect.stack()[0][3]; funcname += '> '
  print(funcname, projname, issueId)

  url = 'https://redmine.biotech.ufl.edu'
  daily = 'day-to-day'
  # proj_issues = url + '/projects/' + daily + '/issues'
  proj_issues = url + '/projects/' + projname + '/issues'
  # default issue is this bash2py task
  task = url + '/issues/' + str(issueId)
  print('this bach2py task-issue in redmine is: ', task)

  # sample pacbio data delivery:
  ddeliv_pacbio = url + '/issues/4814' ; print('PacBio data delivery task-issue: ', ddeliv_pacbio)

  # redmine = Redmine(url, username='david.hon', password='')
  # usind david.hon's redmone api access key (see "My account" page on right panel)
  # redmine = Redmine(url, key='6c0c9e3c80f8ef1e317ba1075a62d01d5cc0ded0')
  proj = _icbred.project.get(projname)
  # print(proj.identifier, proj.created_on, proj.issues[0])
  print(funcname, proj.identifier, proj.created_on)

  cnt = 0
  for task in proj.issues:
    cnt += 1 ; # print(funcname, task)
    # print(funcname, task.id, task.author, task.subject, task.time_entries)
    if( task.id == issueId ):
      print(funcname, cnt, '.) this is my task / issue! ... ')
      print(funcname, task.id, task.author, task.subject, task.time_entries)
  #endfor
  print(funcname, 'total number of issues found for ', projname, ' == ', cnt)

  # dir(proj.issues[0])
  # ['assigned_to', 'author', 'created_on', 'description', 'done_ratio',
  # 'due_date', 'estimated_hours', 'id', 'priority', 'project', 'relations',
  # 'start_date', 'status', 'subject', 'time_entries', 'tracker', 'updated_on']

  return proj
#end projInfo

def taskInfo(issueId=4814):
  url = 'https://redmine.biotech.ufl.edu'
  redmine = Redmine(url, key='6c0c9e3c80f8ef1e317ba1075a62d01d5cc0ded0')
  task = url + '/issues/' + str(issueId)
  info = redmine.issue.get(issueId)
  print(issueId, info)
  return info
# end taskInfo

if __name__ == '__main__':
  projInfo()
