#!/usr/bin/env python
"""
This module provides functions for app. CLI arg parsing, signal handling, zip/unzip,
rsync child proc, and other misc. useful features.

pip install psutil, html2text
"""
import sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

import errno, datetime, getopt, logging, os, signal, time, zipfile
from subprocess import PIPE

import psutil, html2text

import figfont

_childprocs = []

def cliArgs(argv=None):
  """
  usage: icbrpacbio.py [-h --help] infile.xls or infile.csv
  """
  if argv is None:
    argv = sys.argv # etc., replacing sys.argv with argv in the getopt() call.
  # parse command line options
  try:
    opts, args = getopt.getopt(sys.argv[1:], "h", ["help"])
  except getopt.error as msg:
    print(msg)
    return -1
  except err:
    print(err.msg)
    print('for help use -h or --help')
    return -2
  # process options
  for o, a in opts:
    print(o, a)
    if o in ('-h', '--help'): print(cliArgs.__doc__)
#end cliArgs

def sigHandler(signum, frame):
  """
  Simple default sig-handler that prompts to exit or continue processing
  """
  print('Signal handler called with signal', signum)
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    for p in _childprocs:
      print('terminate child proc: ', p.name(), p.username())
      p.terminate()
    #endfif
    sys.exit()
  #endif
#end

def setSigHandler(handler=sigHandler):
  """
  Custom handling of control-z control-c and other signals
  """
  # print('set default signal handler for default signals')
  signal.signal(signal.SIGINT, handler) # control-c
  signal.signal(signal.SIGTSTP, handler) # control-z
  #signal.signal(signal.SIGABRT, handler)
  #signal.signal(signal.SIGCONT, handler)
  #signal.signal(signal.SIGHUP, handler)
  #signal.signal(signal.SIGQUIT, handler)
  #signal.signal(signal.SIGTERM, handler)
#end

def mkdirp(path):
  try:
    os.makedirs(path)
  except OSError as e:
    if e.errno == errno.EEXIST:
      pass
    else:
      print('failed to mkdir -p of '+path)
      raise
#end mkdirp

def spawnRsync(src, dest, excludes=['.svn', '.git']):
  usr = os.environ['USER'] ; print('spawnRsync> user: '+usr)
  if( not os.path.exists(src) ):
    print('sorry, but no such (src) path exists: ', src)
    return usr
  #end
  # if rsync to src local filesystem dest, first try mkdir -p
  if( not ':/' in dest ): mkdirp(dest) # allow this to raise exception
  rsync = ['/usr/bin/rsync', '-lav']
  for exc in excludes:
    rsync.append('--exclude='+exc)
  #endfor
  rsync.append(src) ; rsync.append(dest)
  print('spawnRsync> ', rsync)
  p = psutil.Popen(rsync, stdout=PIPE)
  _childprocs.append(p) # for use in sighandler
  print(p.name(), p.username())
  p.communicate()
  p.wait(timeout=3)
  return usr
#end spawnRsync

def lszip(filename='UF_ICBRcore.zip', pwd='icbr'):
  zf = zipfile.ZipFile(filename, 'r')
  #for fi in zf.infolist():
  #  print(fi)
  for fn in zf.namelist():
    print(fn)
#end

def unzip(filename='UF_ICBRcore.zip', outpath='./tmp', pwd='icbr'):
  zf = zipfile.ZipFile(filename, 'r')
  zf.extractall(outpath, pwd)
#end

def zip(src, dst, pwd='icbr'):
  outfilename = dst
  if( not '.zip' in dst ): outfilename = "%s.zip" % (dst)
  # print('zipping ', src, outfilename)
  zf = zipfile.ZipFile(outfilename, "w", zipfile.ZIP_DEFLATED)
  abs_src = os.path.abspath(src)
  for dirname, subdirs, files in os.walk(src):
    print(dirname)
    try:
      for filename in files:
        filepath = os.path.join(dirname, filename)
        # absname = os.path.abspath(filepath) ; arcname = absname[len(abs_src) + 1:] # print('zipping %s as %s' % (filepath, arcname)
        zf.write(filepath) # zf.write(absname, arcname) # print('zipping %s as %s' % (absname, arcname)
      #endfor
    except: pass
  #endfor
  zf.close()
  lszip(outfilename)
#end zip

def html2txt(htmlfile='DataDelivery.html', txtfile=None):
  try:
    html = open(htmlfile).read()
  except:
    print('Failed to read html file: ', htmlfile)
    return
  #endtry
  txt = html2text.html2text(html)
  if( not txtfile ):
    print(txt)
    return txt
  #endif
  try:
    f = open(txtfile, 'w') ; f.write(txt) ; f.close()
  except:
    print('Failed to write text file: ', txtfile)
  #endtry
  return txt
#end html2txt

"""
An integration test main is provided that tests each function in this module:
./apputil.py -h
"""
if __name__ == "__main__":
  usr = os.environ['USER']
  setSigHandler()
  cliArgs()
  banner = figfont.genBanner()
  obanner = figfont.origBanner()
  d2d = '../../../../day-to-day/2016'
  zip(d2d, '/var/tmp/icbr')
  spawnRsync('/var/tmp/icbr.zip', '/var/tmp/'+usr)

  while( True ):
    print(banner)
    print(obanner)
    print("waiting for signal...\n")
    time.sleep(5.5)
  #endwhile
#endmain
