# redmine issue #4686

Some of the CI bash scripts have become rather lengthy and increasingly difficult to maintain and extend. 
These should be converted to Python with elements refactored into one or more re-usable Python module(s).
http://www.swag.uwaterloo.ca/bash2py/

Jeremy's guide to the data-delivery workflow and script:

https://redmine.biotech.ufl.edu/projects/day-to-day/wiki/USING_THE_NEW_DATA_DELIVERY_SCRIPT

To download or upload readme and/or sha512 files from redmine, try:

https://github.com/maxtepkeev/python-redmine

Non built-in Python modules used:
pip install openpyxl python-redmine pyfiglet

The inspiration for using Sphinx to generate docs from the Python source is from the IBM tutorial:

https://www.ibm.com/developerworks/library/os-sphinx-documentation/


