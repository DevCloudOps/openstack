#!/usr/bin/env python

import os, sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

import hashlib, inspect

def saveShas(outfilename, shasums, verbose=True):
  """
  write the list of { filename : shasum } vals to filename.
  make sure output format is congruent with shasum -a 512 output
  """
  separater = '  '
  f = open(outfilename, 'w')
  allkeys = shasums.keys()
  for name in allkeys:
    text = shasums[name] + separater + name
    if( verbose ): print(text)
    f.write(text + '\n')
  #endfor
  f.close()
  return len(shasums)
#end saveShas

def shasum512(filename, verbose=False):
  """
  read file into buffer and eval sha512
  log result in the same format as shasum -a 512 to be double-checked via shasum -c
  note sym-links may point to non-existant filesystem item
  """
  if(verbose): print(filename)

  filesha = { filename : None }
  try:
    f = open(filename, 'rb')
    s = hashlib.sha512()
    # for really large files may need multiple read-n-update
    buff = f.read() ; s.update(buff)
    shasum = s.hexdigest()
    filesha = { filename : shasum }
    if( verbose ):
      text = shasum + '  ' + flename
      print(textfile)
    #endif
  except OSError as err:
    print("OS error: {0}".format(err))
  except:
    if(verbose): print("non OS error:", sys.exc_info()[0])
    pass
  #end try
  return filesha
#end shasum512

def shasum512list(filelist, logfilename=None, verbose=False):
  """
  evaluate and log sha512 checksums of a list of files, populate and return
  a list of non-None { filename : shasum } hash-dicts.
  """
  shasums = {}
  for filename in filelist:
    filesha = shasum512(filename) # return a single key-val dict
    if(verbose): print('shasum512list> ', filesha)
    if( filesha[filename] ): shasums.update(filesha)
  #endfor
  if( logfilename ):
    cnt = saveShas(logfilename, shasums) ; # print('evaluated ', cnt, ' shasums')
  #endif
  return shasums
#end shasum512list

def fileList(path, verbose=False):
  """
  walk the indicated path hierarchy and return separate lists containing
  files and sub-directorirs, also follow any sym-links present. note that
  os.walk returns an immutable 3-tuple ...
  """
  funcname = inspect.stack()[0][3]; funcname += '> '
  print(funcname, path)
  allfiles = []
  for root, dirs, files in os.walk(path, followlinks=False):
    if(verbose): print('path root:', root, 'found', len(files), 'files')
    for f in files:
      allfiles.append(f)
  #endfor
  return allfiles
#end filelist

if __name__ == "__main__":
  filelist = fileList() ; print(filelist)
  shas = shasum512list(filelist)
  cnt = saveShas('shasums.log', shas) ; # print('evaluated ', cnt, ' shasums')
#endif main
