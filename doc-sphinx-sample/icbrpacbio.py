#!/usr/bin/env python
"""
This module provides one or more functions generating a delivery ReadMe textfile
and perhaps (eventually) more value-added run info., html docs with plots
(SVG or PNG) and tables, that can be viewed via a browser local URL file://

This makes use of custom modules for reading CSV or Excel Data Delivery spreadsheets,
evaluating sha512 checksums, etc.

Use virtualenv to setup python2 or python3 workspace
(sudo yum install -y python-virtualenv or pip install virtualenv):
virtualenv -p /opt/bin/python2.7 --clear --always-copy --prompt=py2env ./py2env ; . ./py2env/bin/activate
virtualenv -p /opt/bin/python3.5 --clear --always-copy --prompt=py3env ./py3env ; . ./py3env/bin/activate

Dependencies: pip install openpyxl python-redmine pyfiglet psutil

Note there seems to no pip installable rsync module, so psutil.Popen
is used to spawn an rsync child proc.
"""
import sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

import csv, datetime, getopt, inspect, logging, os, signal, time
import apputil, checksum, figfont, icbredmine, sprdsheet

def printDoc(doc, outfilename = 'DataDelivery.rst'):
  print('-------------------- begin data-delivery doc --------------------', outfilename)
  print(doc)
  print('--------------------  end data-delivery doc  --------------------', outfilename)
#end printDoc

def genDoc(jobIdNames, shasums, outfilename='DataDelivery.rst', banner=None):
  """
  This is a Title
  ===============
  That has a paragraph about a main subject and is set when the '='
  is at least the same length of the title itself.

  Subject Subtitle
  ----------------
  Subtitles are set with '-' and are required to have the same length
  of the subtitle itself, just like titles.

  Lists can be unnumbered like:

   * Item Foo
   * Item Bar

  Or automatically numbered:

   #. Item 1
   #. Item 2

  Inline Markup
  -------------
  Words can have *emphasis in italics* or be **bold** and you can define
  code samples with back quotes, like when you talk about a command: ``sudo``
  gives you super user powers!
  """
  funcname = inspect.stack()[0][3]; funcname += '> '
  print(funcname, outfilename)

  if(not banner): banner = figfont.origBanner() # original ICBR banner ascii-art

  # the double-colon followed by a new-line and indented text is restructured-text
  # markup syntax for a 'literal block'
  doc = '::\n' + banner + '\n' + genDoc.__doc__
  try:
    f = open(outfilename, 'w')
    f.write(doc)
    f.close()
    print(funcname, 'created markup doc file for Sphinx ... ', outfilename)
    print(funcname, outfilename, ' must be present in sphinx/index.rst before invoking make ...')
  except:
    print(funcname, 'Failed to create markup doc file for Sphinx ... ', outfilename)
  #endtry
  return doc
#end genDoc

def waitloop(banner, interval=5.5):
  # apputil.setSigHandler()
  while( True ):
    print(banner)
    print('waiting for signal...')
    time.sleep(5.5)
  #endwhile
#end waitloop

def jobIdFilelist(path, jobIdNames):
  funcname = inspect.stack()[0][3]; funcname += '> '
  names = jobIdNames.values()
  print(funcname, names)

  allfiles = checksum.fileList(path) ; # print(allfile)
  # set filelist with from allfiles-list that match (job) names-list ...
  filelist = []
  for file in allfiles:
    for name in names:
      if name in file:
        filelist.append(name)

  return filelist
#end jobIdFilelist

"""
An InT main tests functions in this module, as well as imported custom modules:
./icbrpacbio.py -h
"""
if __name__ == '__main__':
  apputil.cliArgs()
  banner = figfont.genBanner() ; print(banner)
  obanner = figfont.origBanner()
  redproj = icbredmine.projInfo()

  jobIdNames = { 'aJobId' : 'a jobname', 'bJobId' : 'b jobname' }
  # jobIdNames = sprdsheet.parseCSV('pacbio_tom_spencer.csv')
  jobIdNames = sprdsheet.parseXcel('pacbio_tom_spencer.xlsx')
  print('job count: ', len(jobIdNames)) ; print('job names:\n', jobIdNames)

  filelist = jobIdFilelist('.', jobIdNames)
  # shasums can take awhile, so allow for graceful interrupt-terminate via
  # control-c or control-z
  # apputil.setSigHandler()

  shasums = checksum.shasum512list(filelist, 'shasums.log')
  outfilename = 'DataDelivery.rst'
  doc = genDoc(jobIdNames, shasums, outfilename)
  print(doc)
  print('Reminder ... ', outfilename, ' must be present in sphinx/index.rst before invoking make ...')
  print('Reminder ... ', outfilename, ' must be present in sphinx/index.rst before invoking make ...')
  print('Reminder ... ', outfilename, ' must be present in sphinx/index.rst before invoking make ...')

  # waitloop(obanner)
#endmain
