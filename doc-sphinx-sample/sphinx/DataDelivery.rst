::

  0===0
   O=o
    O
   O=o
  0===0  8888888 .d8888b.  888888b.   8888888b.
  0===0    888  d88P  Y88b 888  '88b  888   Y88b
   O=o     888  888    888 888  .88P  888    888
    O      888  888        8888888K.  888   d88P
   o=O     888  888        888  'Y88b 8888888P'
  0===0    888  888    888 888    888 888 T88b
  0===0    888  Y88b  d88P 888   d88P 888  T88b
   O=o   8888888 'Y8888P'  8888888P'  888   T88b
    O
   o=O           www.biotech.ufl.edu
  0===0

  2033 Mowry Road, Gainesville, FL 32610 (352) 273-8030
  

This is a Title
===============
That has a paragraph about a main subject and is set when the '='
is at least the same length of the title itself.

Subject Subtitle
----------------
Subtitles are set with '-' and are required to have the same length
of the subtitle itself, just like titles.

Lists can be unnumbered like:

 * Item Bar
 * Item Food
 * Item Dine

Or automatically numbered:

 #. Item 1
 #. Item 2
 #. Item 3
 #. Item 4
 #. Item 5

Inline Markup
-------------
Words can have *emphasis in italics* or be **bold** and you can define
code samples with back quotes, like when you talk about a command: ``sudo``
gives you super user powers!
  
