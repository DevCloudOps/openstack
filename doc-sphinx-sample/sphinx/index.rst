.. Data Delivery Scripts documentation master file, created by
   sphinx-quickstart on Thu Mar 24 15:34:59 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Data Delivery Scripts's documentation!
=================================================

Contents:

.. toctree::
   :maxdepth: 2

   DataDelivery

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

