#!/bin/bash

##### v2.0 #####

##### Constants
FIND='/usr/bin/find'

USAGE="Usage: $( basename $0 ) -P [Platform] -R [Run Directory directory] -D [optional subdirectory] -M [Delivery Mount Point]"

# Gather Info

#### GET PI NAME
echo "Type the Name of the PI, followed by [ENTER]:"
read PINAME

# Process command-line arguments using getopt
while getopts ":P:R::D::M::" OPT; do
  case $OPT in
    P ) echo "You entered the ${OPTARG} platform"
		if [ "${OPTARG}" != "" ]; then PLATFORM="${OPTARG}"
		    #echo "parameter 1 contains something"
		else
		    echo "${USAGE}"
		fi
        ;;
    R ) echo "You entered run ${OPTARG}."
		if [ "${OPTARG}" != "" ]; then RUNDIR="${OPTARG}"
		    #echo "parameter 2 contains something"
		else
		    echo "${USAGE}"
		fi
        ;;
    D ) echo "You entered sub direcotry ${OPTARG}."
		if [ "${OPTARG}" != "" ]; then SUBDIR="${OPTARG}"
		    #echo "parameter 3 contains something"
		else
		    echo "${USAGE}"
		fi
		;;
    M ) echo "You entered delivery mount point ${OPTARG}."
		if [ "${OPTARG}" != "" ]; then MOUNT="${OPTARG}"
		    #echo "parameter 3 contains something"
		else
		    echo "${USAGE}"
		fi
		;;
  esac
done

echo " You want to deliver "${PLATFORM}" data "${RUNDIR}"/"${SUBDIR}" to "${PINAME}" to location "${MOUNT}" "


##### FUNCTIONS ##### 

echo  "Genertaing Readme File, please wait..."
 
#####
 # Create ReadMe
#####

HEADER="
  0===0 
   O=o
    O
   O=o
  0===0  8888888 .d8888b.  888888b.   8888888b.
  0===0    888  d88P  Y88b 888  '88b  888   Y88b 
   O=o     888  888    888 888  .88P  888    888 
    O      888  888        8888888K.  888   d88P 
   o=O     888  888        888  'Y88b 8888888P'
  0===0    888  888    888 888    888 888 T88b  
  0===0    888  Y88b  d88P 888   d88P 888  T88b  
   O=o   8888888 'Y8888P'  8888888P'  888   T88b
    O
   o=O           www.biotech.ufl.edu
  0===0  

2033 Mowry Road, Gainesville, FL 32610 (352) 273-8030



LOCATING "${PLATFORM}" FILES
--------------------
This project's files are located within the following project directory:

"${RUNDIR}"/"${SUBDIR}"

LOCATING THE CHECKSUM FILE
--------------------------
There is a single SHA512 checksum file for all files delivered from ${RUNDIR} for this project.

The checksum file is located at the root of the project folder:

"${PINAME}"."${PLATFORM}".sha512

DIRECTORY STRUCTURE OF FILES DELIVERED			
-------------------------------------------------------------------------------------------------------------------

DELIVERABLE DIRECTORY OF "${RUNDIR}"/"${SUBDIR}" "

   		if [ "${PLATFORM}" = "Pacbio" ]; then FILE_LIST="$( find "${RUNDIR}""${SUBDIR}" -name "*.h5" -o -name "*.metadata.xml" -o -name "*.fasta" -o -name "*.fastq")"
		    #echo "${FILE_LIST}"
		else
		    FILE_LIST="$(find "${RUNDIR}" -type f -name '*.fastq.gz')"
		    #echo "${FILE_LIST}"
		fi

PRINT_LIST="$( echo "${FILE_LIST}" | sed 's|.*"${RUNDIR}"||')"



echo "${HEADER}" > /tmp/"${RUNDIR}"."${PINAME}"."${PLATFORM}".ReadMe.txt
echo "${PRINT_LIST}"  >> /tmp/"${RUNDIR}"."${PINAME}"."${PLATFORM}".ReadMe.txt

scp /tmp/"${RUNDIR}"."${PINAME}"."${PLATFORM}".ReadMe.txt "${MOUNT}"/


######
 # Create sha512
#######

 echo "Genertaing checksum file, please wait..."

		if [ "${PLATFORM}" = "Pacbio" ]; then 
			find "${RUNDIR}" -type f -name "*.h5" -o -name "*.metadata.xml" -o -name "*.fasta" -o -name "*.fastq" | xargs shasum -a 512 > /tmp/"${RUNDIR}"."${PINAME}"."${PLATFORM}".sha512
		else
			find "${RUNDIR}" -type f -name '*.fastq.gz'| xargs shasum -a 512 > /tmp/"${RUNDIR}"."${PINAME}"."${PLATFORM}".sha512
		fi

scp /tmp/"${RUNDIR}"."${PINAME}"."${PLATFORM}".sha512 "${MOUNT}"/

#####
 # Rsync data
#####

 #echo "Rsyncing Data please wait..."


 		#if [ "${PLATFORM}" = "Pacbio" ]; then echo "Rsyncing ${PLATFORM} Data please wait..."
 			#nohup rsync -av --exclude '*.log' --exclude '*.sts.xml' --exclude '*.csv' --exclude '*.xfer.xml' "${RUNDIR}" "${MOUNT}"/ 2>&1 > /tmp/rsync."${PLATFORM}".$(date +%Y-%m-%d).deliverhd."${RUNDIR}".log
		#else
		 	#nohup rsync -av --include '*fastq.gz' --exclude '*.*' --exclude '*/' --exclude '*/*'  "${RUNDIR}" "${MOUNT}"/ 2>&1 > /tmp/rsync."${PLATFORM}".$(date +%Y-%m-%d).deliverhd."${RUNDIR}".log
		#fi

sleep 15
echo "Data Delivery Complete"
exit 0