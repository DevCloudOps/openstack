#!/usr/bin/env python

import sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

from pyfiglet import Figlet

"""
Use Pyfiglet, which is allegedly a direct port of figlet and hopefully supports
the same fonts found at http://www.figlet.org/fontdb.cgi:
"""

def genBanner(text='ICBR\nCyber\nInfrastructure', bannerfont='small'):
  """
  Generate ASCII art banner with the indicated font.
  See allfonts() func. to list available fonts families.
  """
  f = Figlet(font=bannerfont)
  banner = f.renderText(text) # print(banner)
  return banner
#end genBanner

def allfonts():
  """
  List of ~147 font names, cut-n-pasted from http://www.figlet.org/fontdb.cgi
  """
  all = (
  "3-d",
  "3x5",
  "5lineoblique",
  "acrobatic",
  "alligator",
  "alligator2",
  "alphabet",
  "avatar",
  "banner",
  "banner3-D",
  "banner3",
  "banner4",
  "barbwire",
  "basic",
  "bell",
  "big",
  "bigchief",
  "binary",
  "block",
  "bubble",
  "bulbhead",
  "calgphy2",
  "caligraphy",
  "catwalk",
  "chunky",
  "coinstak",
  "colossal",
  "computer",
  "contessa",
  "contrast",
  "cosmic",
  "cosmike",
  "cricket",
  "cursive",
  "cyberlarge",
  "cybermedium",
  "cybersmall",
  "diamond",
  "digital",
  "doh",
  "doom",
  "dotmatrix",
  "drpepper",
  "eftichess",
  "eftifont",
  "eftipiti",
  "eftirobot",
  "eftitalic",
  "eftiwall",
  "eftiwater",
  "epic",
  "fender",
  "fourtops",
  "fuzzy",
  "goofy",
  "gothic",
  "graffiti",
  "hollywood",
  "invita",
  "isometric1",
  "isometric2",
  "isometric3",
  "isometric4",
  "italic",
  "ivrit",
  "jazmine",
  "jerusalem",
  "katakana",
  "kban",
  "larry3d",
  "lcd",
  "lean",
  "letters",
  "linux",
  "lockergnome",
  "madrid",
  "marquee",
  "maxfour",
  "mike",
  "mini",
  "mirror",
  "mnemonic",
  "morse",
  "moscow",
  "nancyj-fancy",
  "nancyj-underlined",
  "nancyj",
  "nipples",
  "ntgreek",
  "o8",
  "ogre",
  "pawp",
  "peaks",
  "pebbles",
  "pepper",
  "poison",
  "puffy",
  "pyramid",
  "rectangles",
  "relief",
  "relief2",
  "rev",
  "roman",
  "rot13",
  "rounded",
  "rowancap",
  "rozzo",
  "runic",
  "runyc",
  "sblood",
  "script",
  "serifcap",
  "shadow",
  "short",
  "slant",
  "slide",
  "slscript",
  "small",
  "smisome1",
  "smkeyboard",
  "smscript",
  "smshadow",
  "smslant",
  "smtengwar",
  "speed",
  "stampatello",
  "standard",
  "starwars",
  "stellar",
  "stop",
  "straight",
  "tanja",
  "tengwar",
  "term",
  "thick",
  "thin",
  "threepoint",
  "ticks",
  "ticksslant",
  "tinker-toy",
  "tombstone",
  "trek",
  "tsalagi",
  "twopoint",
  "univers",
  "usaflag",
  # "wavy",
  "weird"
  )
  return all
#end allfonts

def origBanner():
  """
  0===0
   O=o
    O
   O=o
  0===0  8888888 .d8888b.  888888b.   8888888b.
  0===0    888  d88P  Y88b 888  '88b  888   Y88b
   O=o     888  888    888 888  .88P  888    888
    O      888  888        8888888K.  888   d88P
   o=O     888  888        888  'Y88b 8888888P'
  0===0    888  888    888 888    888 888 T88b
  0===0    888  Y88b  d88P 888   d88P 888  T88b
   O=o   8888888 'Y8888P'  8888888P'  888   T88b
    O
   o=O           www.biotech.ufl.edu
  0===0

  2033 Mowry Road, Gainesville, FL 32610 (352) 273-8030
  """
  return origBanner.__doc__
#end origBanner

if __name__ == "__main__":
  flist = allfonts()
  print('total number of fonts: ', len(flist))
  print(flist)
  cnt = 0
  for f in flist:
    cnt += 1
    b = genBanner(bannerfont=f)
    print(cnt, f)
    print(b)

  o = origBanner()
  print(o)
