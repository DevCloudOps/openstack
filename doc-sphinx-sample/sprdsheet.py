#!/usr/bin/env python
"""
This module provides functions for reading CSV or Excel Data Delivery spreadsheets.
The Data Delivery Metrics sheet contains two columns of interest: "jobid - Job ID" and "jobname - Job Name"
The Data Delivery Job sheet also provides two columns of interest: "jobId" and "name"

Dependencies: pip install openpyxl

use virtualenv to setup python2 or python3 workspace (sudo yum install -y python-virtualenv or pip install virtualenv):
virtualenv -p /opt/bin/python2.7 --clear --always-copy --prompt=py2env ./py2env ; . ./py2env/bin/activate
virtualenv -p /opt/bin/python3.5 --clear --always-copy --prompt=py3env ./py3env ; . ./py3env/bin/activate
"""
import sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

import csv, datetime, inspect, logging, os, traceback
from openpyxl import load_workbook
import apputil

def parseCSV(filename='pacbio_tom_spencer.csv'):
  """
  Fetch Job Ids, Names, and any other info. from a CSV file.
  """
  funcname = inspect.stack()[0][3]; funcname += '> '
  print(funcname, filename)
  sheet = None
  try:
    f = open(filename)
    sheet = list(csv.reader(f))
    f.close()
  except:
    print(funcname, 'failed to read: ', filename)
    traceback.print_last()
    return None
  #end try
  row0 = sheet.pop(0) ; # print(row0)
  jId_idx = row0.index('jobid - Job ID')
  jname_idx = row0.index('jobname - Job Name')
  jobIdNames = {} # { 'aJobId' : 'a jobname', 'bJobId' : 'b jobname', 'cJobId' : 'c jobname' }
  for row in sheet:
    # print(cnt, '.) ', row)
    jIdName = { row[jId_idx] : row[jname_idx] }
    jobIdNames.update(jIdName)
  #endfor
  print(funcname, 'number of jobs found: ', len(jobIdNames))
  return jobIdNames
#end parseCVS

def dumpXcelSheets(wsheets):
  sheetnum = 0
  rownum = 0
  for sheet in wsheets:
    sheetnum += 1
    rownum = 0
    for row in sheet.rows:
      rownum += 1
      cellnum = 0
      text = str(sheetnum) + '-' + str(rownum) + '.) '
      sys.stdout.write(text)
      for cell in row:
        cellnum += 1
        text = str(cellnum) + ': ' + repr(cell.value)
        sys.stdout.write(text)
      #endfor cells
      sys.stdout.write('\n')
    #endfor rows
  #endfor wsheets
#end dumpXcelSheets()

def xcelWorkBook(filename):
  funcname = inspect.stack()[0][3]; funcname += '> '
  print(funcname, filename)

  try:
    wb = load_workbook(filename) # Workbook()
  except:
    print(funcname, 'failed to read: ', filename)
    traceback.print_last()
    return None
  #endtry

  # more example api usage
  # grab the active worksheet, or by index or name
  ws1 = wb.active
  ws1 = wb.worksheets[0]
  #ws2 = wb.worksheets[1] # wb.create_sheet()
  #ws1 = wb.get_sheet_by_name("Sheet1")
  #ws2 = wb.get_sheet_by_name("Sheet2")

  # Data can be assigned directly to cells
  # ws['A1'] = 42
  # Python types will automatically be converted
  # ws['A2'] = datetime.datetime.now()
  # Rows can also be appended
  # ws.append([1, 2, 3])

  # Save the file
  # wb.save("sample.xlsx")
  # how many sheets are in the workbook?
  wsheets = []
  for sheet in wb:
    print(funcname, 'sheet title: ', sheet.title)
    wsheets.append(sheet)
  # end
  print("number of sheets: ",len(wsheets))
  return wsheets
#end xcelWorkBook

def allXcells(row):
  cells = []
  for cell in row: cells.append(cell.value)
  return cells

def allXcelRows(sheet):
  rowvals = []
  rowcnt = sheet.get_highest_row()
  rows = list(sheet.iter_rows())
  row0 = allXcells(rows[0]) ; print(row0)
  for row in rows:
    rowvals.append(allXcells(row))
  return rowvals
#end allXcelRows

def parseXcel(filename='pacbio_tom_spencer.xlsx'):
  """
  Fetch Job Ids, Names, and any other info. from one or more Excel spreadsheet(s).
  """
  funcname = inspect.stack()[0][3]; funcname += '> '
  print(funcname, filename)

  wsheets = xcelWorkBook(filename)
  sheet = wsheets[0] ; print(funcname, sheet.title)

  rows = allXcelRows(sheet)
  row0 = rows.pop(0) ; # print(row0)
  jId_idx = row0.index('jobid - Job ID')
  jname_idx = row0.index('jobname - Job Name')

  jobIdNames = {} # { 'aJobId' : 'a jobname', 'bJobId' : 'b jobname', 'cJobId' : 'c jobname' }
  for row in rows:
    jIdName = { row[jId_idx] : row[jname_idx] }
    jobIdNames.update(jIdName)
  #endfor
  print(funcname, 'number of jobs found: ', len(jobIdNames))
  return jobIdNames
#end parseXcel

"""
An integration test main is provided that tests functions in this module,
and any imported (custom) modules:
./sprdsheet.py -h
"""
if __name__ == '__main__':
  apputil.setSigHandler()
  apputil.cliArgs()
  # test defaults:
  csvjobs = parseCSV()
  xceljobs = parseXcel()
  print(__name__, ' number of jobs found: csv, xcel', len(csvjobs), len(xceljobs))
#endmain
