#!/bin/bash

FIND="/usr/bin/find"
USAGE="Usage: $( basename $0 ) -P [Platform] -R [Instrument Run Data] -A [Analysis Data] -M [Delivery Mount Point]"

###############
# Process command-line arguments using getopt
###############

while getopts ":P::R::A::M::" OPT; do
  case $OPT in
    P ) #echo "You entered the ${OPTARG} platform"
		if [ "${OPTARG}" != "" ]; then PLATFORM="${OPTARG}"
		else
		    echo "${USAGE}"
		fi
        ;;
    R ) # echo "You entered the ${OPTARG} runlist"
		if [ "${OPTARG}" != "" ]; then RUNLIST="${OPTARG}"
		else
		    echo "${USAGE}"
		fi
        ;;
    A ) # echo "You entered sub direcotry " cat "${OPTARG}."
		if [ "${OPTARG}" != "" ]; then ANALIST="${OPTARG}"
		else
		    echo "${USAGE}"
		fi
		;;
    M ) #echo "You entered delivery mount point ${OPTARG}."
		if [ "${OPTARG}" != "" ]; then MOUNT="${OPTARG}"
		else
		    echo "${USAGE}"
		fi
		;;
  esac
done

###############
# VARIBALE SET BY PLATFORM
###############

if [ "${PLATFORM}" = "Pacbio" ]; then
  RUNPATH='/data/pacbio/rs/rundata' 
  ANALPATH='/data/pacbio/rs/userdata/jobs/018/0'
  SERVER='s1-0'
elif [ "${PLATFORM}" = "Illumina.NextSeq" ]; then
  RUNPATH='/uf/icbr/Resources/Instruments/Covaris S2/Nextseq_500_sharetest/' 
  ANALPATH='NextSeq BaseSpace or C7-0:'
  SERVER='s2-0'
elif [ "${PLATFORM}" = "Illumina.MiSeq" ]; then
  RUNPATH='/uf/icbr/Resources/Instruments/Illumina MiSeq/' 
  ANALPATH='MiSeq BaseSpace or C7-0:'
  SERVER='s2-0'
fi

###############
# GET PI NAME
###############

echo "Type the Name of the PI, followed by [ENTER]:"
read PINAME

READMEFILE="${PINAME}.${PLATFORM}.$(date +%Y-%m-%d).ReadMe.txt"
SHAFILE="${PINAME}.${PLATFORM}.$(date +%Y-%m-%d).sha512"
###############
# Double Check OUTPUT
###############
		
      echo "."   
      sleep 1
      echo ".." 
      sleep 1 
   		echo "You want to deliver Platform: ${PLATFORM} to PI: ${PINAME}"
      sleep 1
      echo "..."
      echo "RUNS DIRECTORIES:" 
        cat "${RUNLIST}"
      sleep 1
      echo "...."
      echo "ANALYSIS DIRECTORIES:" 
        cat "${ANALIST}"
      sleep 1 
      echo "....."
      echo "DELIVERY LOCATION ${MOUNT}"
      sleep 1
      echo "......."
      echo "PATH TO RUN ORINIGNALS ${RUNPATH}" 
      sleep 1
      echo "......."
      echo "PATH TO ANALYSIS ORINIGNALS ${ANALPATH}" 
      sleep 1
      echo  "........"
      echo "Remote Server ${SERVER}"
      sleep 1
      echo "........."
  
  echo "Do you wish to continue?, [Y/N]:"
  read PROCEED

   if [ "${PROCEED}" = "N" ]; then exit
               echo "do not continue" 
               exit 1
   fi

#################
 #Clear /tmp Files
#################

> /tmp/"${READMEFILE}"
> /tmp/"${SHAFILE}"

#################
#################
# Create ReadMe #
#################
#################

echo  "Genertaing Readme File, please wait..."

#####

HEADER="
  0===0 
   O=o
    O
   O=o
  0===0  8888888 .d8888b.  888888b.   8888888b.
  0===0    888  d88P  Y88b 888  '88b  888   Y88b 
   O=o     888  888    888 888  .88P  888    888 
    O      888  888        8888888K.  888   d88P 
   o=O     888  888        888  'Y88b 8888888P'
  0===0    888  888    888 888    888 888 T88b  
  0===0    888  Y88b  d88P 888   d88P 888  T88b  
   O=o   8888888 'Y8888P'  8888888P'  888   T88b
    O
   o=O           www.biotech.ufl.edu
  0===0  

2033 Mowry Road, Gainesville, FL 32610 (352) 273-8030


LOCATING ${PLATFORM} FILES
--------------------

Instrument files are located within the following directory:

`cat ${RUNLIST}`

Analysis files are located within the following directory:

`cat ${ANALIST}`

LOCATING THE CHECKSUM FILE
--------------------------
There is a single SHA512 checksum file for all files delivered for this project.

The checksum file is located at the root of the project folder:

"${SHAFILE}"

DIRECTORY STRUCTURE OF FILES DELIVERED			
-------------------------------------------------------------------------------------------------------------------

DELIVERABLE DIRECTORY OF "${PINAME}"'s Project' 

Instrument Data:"
########
 # END HEADER
#######

###
#INSERT HEADER
###

echo "${HEADER}" >> /tmp/"${READMEFILE}"

echo "Do you wish to continue?, [Y/N]:"
  read PROCEED

   if [ "${PROCEED}" = "N" ]; then exit
               echo "do not continue" 
               exit 1
   fi

   	while read line; do
   		find "${line}" -type f >> /tmp/"${READMEFILE}"
	  done < "${RUNLIST}"

#####################
 # Adding "Analysis Data:" to ReadME.txt ####
 
echo "" >> /tmp/"${READMEFILE}"
echo "Analysis Data:" >> /tmp/"${READMEFILE}"

#####################
 # Adding Analysis File LIst to Readme.txt ####
#####################

  while read line; do
      find "${line}" -type f >> /tmp/"${READMEFILE}"
  done < "${ANALIST}"

scp /tmp/"${READMEFILE}" "${MOUNT}"/

#####################
#####################
 # Create sha512
#####################
#####################

 echo "Genertaing checksum file, please wait..."

> /tmp/"${SHAFILE}"

	while read line; do
  echo 		"find "${RUNPATH}"/"${line}" | xargs sha512sum  >> /tmp/"${SHAFILE}" " 
	done < "${RUNLIST}"

  while read line; do
  echo    "find "${ANALPATH}"/"${line}" | xargs sha512sum  >> /tmp/"${SHAFILE}""
  done < "${ANALIST}"

scp /tmp/"${SHAFILE}" "${MOUNT}"/

################
################
 # Rsync data #
################
################

 echo "Rsyncing ${PLATFORM} Data please wait..."

 	while read line; do  
  echo 		"nohup rsync -av "${line}" "${MOUNT}"/ 2>&1 > /tmp/rsync."${PLATFORM}"."$(date +%Y-%m-%d)".deliverhd.ICBRPROJECT.log;"    
	done < "${RUNLIST}"
 
  while read line; do
  echo    "nohup rsync -av "${line}" "${MOUNT}"/ 2>&1 > /tmp/rsync."${PLATFORM}"."$(date +%Y-%m-%d)".deliverhd.ICBRPROJECT.log;" 
  done < "${ANALIST}"

echo "Data Delivery Complete"
exit 0

#----------
################

