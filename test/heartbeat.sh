#!/bin/sh
# write both stderr and stdout to log-file and tty:
# ./heartbeat.sh 2>&1 | tee -a heartbeat.log
# cat -n heartbeat.log | egrep -i 'abor|erro|excep|fail|fata|unexpect' |  grep -vi connect | more

function htoplog {
  dtime=`date "+%Y-%j-%H:%M"`
  htlog=`pwd`/htop-${dtime}.log
  touch $htlog
  echo '=========================================================== htop | aha | html2text ===================================================='
  echo q | htop | aha --black --line-fix | html2text >> $htlog
  echo '======================================================================================================================================='
}


function find_files {
  path=`pwd`
  if [ $1 ] ; then path="$1" ; fi

  echo find Gigabyte size files ...
  \ls -1lhqR $path | grep 'G '
}

function logstat {
  dblog=/var/lib/mysql/queries.log
  if [ $1 ] ; then dblog="$1" ; fi

  pulselog=`pwd`/heartbeat.log
  if [ $2 ] ; then pulselog="$2" ; fi

  wc=`egrep -i 'abort|cancel|error|exceed|exception|fail|fatal|interrupt|kill' $dblog | grep -v WHERE | grep -v SELECT | wc -l`
  if [ $wc > 0 ] ; then
    echo dbstat found $wc potential exceptions in mariadb query log: $dblog
  fi

  wc=`egrep -i 'abort|cancel|error|exceed|exception|fail|fatal|interrupt|kill' /var/log/mariadb/mariadb.log | wc`
  if [ $wc > 0 ] ; then
    echo dbstat found $wc potential exceptions in mariadb log: /var/log/mariadb/mariadb.log
  fi

  wc=`egrep -i 'abort|cancel|error|exceed|exception|fail|fatal|interrupt|kill' $pulselog | wc`
  if [ $wc > 0 ] ; then
    echo dbstat found $wc potential exceptions in heartbeat log: $pulselog
  fi
}


  
# sighandler to end infinite loop:
trap 'echo you hit Ctrl-C, now exiting..; exit' SIGINT SIGTERM

invoke=$_
string="$0"
subshell=${string//[-._]/}

if [ "$subshell" == "bash" ]; then
  echo this script must NOT be sourced
  echo try: \"'./'heartbeat.sh}\" ... or: \"sh hearbeat.sh\"
  return
fi

export OS_PROJECT_DOMAIN_NAME=default
export OS_USER_DOMAIN_NAME=default
export OS_PROJECT_NAME=admin
export OS_USERNAME=admin
#export OS_PASSWORD=cloud
export OS_PASSWORD=oct2017cloud
export OS_AUTH_URL=http://controller:35357/v3
export OS_IDENTITY_API_VERSION=3
export OS_IMAGE_API_VERSION=2

pulse=10
echo check neutron and nova services every $pulse seconds

while [ true ] ; do 
  echo mariadb connections and such ...
  mysql -pcloud -e "show status like '%onn%';" | egrep -i 'abort|max'
  mysql -pcloud -e "show global status like '%connections%';" | egrep -i 'abort|max'
  mysql -pcloud -e "show variables like '%connection%';" | grep -i max | egrep -i 'abort|max'
  echo ------------------------- end mariadb check ---------------------------

  echo ''
  echo check neutron via openstack network and subnet list
  openstack network list;  openstack subnet list
  echo ------------------------- end neutron check ---------------------------
  echo ''
  echo check nova via openstack host and hypervisor and flavor and server list
  openstack host list; openstack hypervisor list 
  openstack flavor list; openstack server list 
  echo -------------------------   end nova check  ---------------------------
  echo ''
  echo sleep $pulse sec. ...
  sleep $pulse
done

