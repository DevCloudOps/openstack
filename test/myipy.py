#!/usr/bin/env python

import os
from IPython.terminal.prompts import Prompts, Token

class MyPrompt(Prompts):
  def in_prompt_tokens(self, cli=None):
    return [(Token, os.getcwd()), (Token.Prompt, ' >>>')]

def myipy():
  ip = get_ipython()
  ip.prompts = MyPrompt(ip)

