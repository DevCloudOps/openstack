#!/bin/sh

echo http://docs.openvswitch.org/en/latest/faq/issues/

function ovsbuild {
  ./configure --prefix=/usr --localstatedir=/var --sysconfdir=/etc --with-linux=/lib/modules/$(uname -r)/build

  make
}

function ovsstat {
  systemctl status openvswitch
  systemctl status ovn-controller
  systemctl status ovs-vswitchd
  systemctl status ovsdb-server
}

function ovsinstall {
  systemctl stop openvswitch
  systemctl stop ovn-controller

  make install

  systemctl restart openvswitch
# systemctl restart ovn-controller
}

function ovsreset {
  ovs-vsctl emer-reset
  systemctl restart openvswitch
# systemctl restart ovn-controller
  ovsstat -v
}

function mvip2ovsbr {
  nic=eth1
  if [ $1 ] ; then nic="$1" ; fi

  ovsbr=ovsbr1
  if [ $2 ] ; then ovsbr="$2" ; fi

  ip=172.17.1.1
  ipnic=`ip neigh|grep $nic|awk '{print $1}'`
  echo NIC $nic IP is ${ip}
  if [[ $ipnic ]] ; then
    echo NIC $nic IP is $ipnic
    ip=${ipnic}
  else
    echo NIC $nic lacks IP, nothing to move to OVS bridge.
    return
  fi

  echo moving NIC $nic IP ${ip} to OVS bridge $ovsbr

  ip addr flush dev $nic
  ovs-vsctl add-br $ovsbr
  ovs-vsctl add-port $ovsbr $nic

  ip addr add ${ip}/12 dev $ovsbr
  ip link set $ovsbr up
}

ovsstat -v

