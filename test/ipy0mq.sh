#!/usr/bin/env python

# https://stackoverflow.com/questions/39055839/how-to-start-ipython-kernel-and-connect-using-zmq-sockets
# https://stackoverflow.com/questions/39055839/how-to-start-ipython-kernel-and-connect-using-zmq-sockets
# python3 -m ipykernel install --user

# start a kernel in the current session:

import IPython

IPython.start_kernel(argv=[])


# start a kernel in a separate child? proc
import jupyter_client

kernel_manager, kernel_client = jupyter_client.start_new_kernel(kernel_name='python3')
print(kernel_manager.get_connection_info())


