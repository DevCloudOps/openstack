#!/bin/sh
# https://stackoverflow.com/questions/38830610/access-jupyter-notebook-running-on-docker-container
# After your docker run command, a hyperlink should be automatically generated.
# It looks something like this: http://localhost:8888/?token=f3a8354eb82c92f5a12399fe1835bf8f31275f917928c8d2 :: /home/jovyan/work

# https://docs.docker.com/engine/reference/run/#clean-up-rm

nbdir= /var/tmp/${USER}_notebooks # `pwd`/notebooks
\mkdir -p $nbdir >& /dev/null
chmod a+rwx $nbdir

name=scipynb # or running container Id, as opposed to image name or Id

# (re)start stopped container or resume paused one:
echo docker start $name # scipynb
echo docker unpause $name # scipynb

# first time run:
echo docker run -dit -p 8088:8888 -v "${nbdir}:/notebooks" --name $name jupyter/scipy-notebook start-notebook.sh --NotebookApp.token=''

name=jupytrnb
#echo docker run --rm -it -p 9999:8888 -v "${nbdir}:/notebooks" --name $name jupyter/notebook jupyter notebook -i 0.0.0.0:8888
# --rm=false: Automatically remove the container when it exits (incompatible with -d)
echo docker run -dit -p 9999:8888 -v "${nbdir}:/notebooks" --name $name jupyter/notebook jupyter notebook -i 0.0.0.0:8888

echo to get the link again later down the linu:
echo docker exec -it $name jupyter notebook list

echo get list of any/all jupyter notebook kernels
echo docker exec -it $name jupyter kernelspec list

function dockinstall {
  systemctl stop docker
# yum remove docker-ce
# yum install docker-ce
  yum update docker-ce
  groupadd docker
  ulist="$USER ben david david.hon jermy omar spence"
  for u in $ulist ; do usermod -aG docker $u ; done
  systemctl restart docker
}

function dockinfo {
  docker --version
  docker ps -a 
# docker images -a --digests
  docker images -a --no-trunc
  \ls -al /var/run/docker /var/lib/docker
}

function dockdel {
  docker pause $nameOrId
  docker unpause $nameOrId
  docker stop $nameOrId # scipynb
  docker rm -f $nameOrId
  docker rmi $imageNameOrId # jupyter/scipy-notebook
}


