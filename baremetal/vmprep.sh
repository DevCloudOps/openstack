#!/bin/sh

# http://libguestfs.org/virt-sysprep.1.html
# http://libguestfs.org/virt-builder.1.html#users-and-passwords
 
function vmprep {
  qcow=`pwd`/VMs/Fedora-Cloud-Base-26-1.5.x86_64.qcow2
  if [ $1 ] ; then qcow="$1" ; fi

  pwd=cloudcloud
  if [ $2 ] ; then pwd="$2" ; fi

  distro=fedora
  if [ $3 ] ; then distro="$3" ; fi

  virt-sysprep -a $qcow --root-password password:${pwd}
  virt-sysprep -a $qcow --password ${distro}:password:${pwd}
}

# virt-sysprep -a $qcow --firstboot-command 'useradd -m -p "" hon ; chage -d 0 hon'
virt-sysprep -a $qcow --firstboot-command 'useradd -m -p "" hon'

function vmsize {
  sz=32G
  if [ $1 ] ; then sz="$1" ; fi
  if [ $2 ] ; then vm="$2" ; fi

  qcow=`pwd`/${vm}.qcow2
  newq=new-${vm}.qcow2

  fdev=/dev/sda1
  qemu-img create -f qcow2 -o preallocation=metadata ${newq} $sz
  virt-resize --expand $fdev $qcow ${newq}
  \ls -alqF ${newq}
}

function vmsave {
  vcpu=8
  if [ $3 ] ; then vcpu="$3" ; fi

  vram=8192 # 1024 # 2048
  if [ $4 ] ; then vram="$4" ; fi

  if [ $1 ] ; then echo "CPUs: $vcpu" "RAM: $vram" $vm ; fi

  vname=${vm}-ram${vram}-cpu${vcpu}

  virsh managedsave $vname --bypass-cache --paused
  virsh list
}

vm=CentOS-7-x86_64-GenericCloud-1708
#vm=Fedora-Cloud-Base-26-1.5.x86_64
vmsize 32G $vm

