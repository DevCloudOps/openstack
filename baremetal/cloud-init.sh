#!/bin/sh
# http://cloudinit.readthedocs.io/en/latest/topics/datasources/nocloud.html
# http://cloudinit.readthedocs.io/en/latest/index.html
# ubuntu 16:  https://www.ibm.com/support/knowledgecenter/en/SSB27U_6.4.0/com.ibm.zvm.v640.hcpo5/instsubuntu.htm
# centos-7 notes
systemctl mask cloud-final.service
systemctl mask cloud-config.service

wkdir=config_drive/openstack/latest
mkdir -p $wkdir


metahost=${wkdir}/meta_data.json

userdata=${wkdir}/user_data


# Edit the 2 config files to contain:
echo '{ "uuid": "atomic.90ghz.org" }' > $metahost


# and

echo '#cloud-config' > $userdata
echo 'users:' >> $userdata
echo '  - name: hon' >> $userdata
echo '    gecos: full user name for account' >> $userdata
echo '    sudo: ["ALL=(ALL) NOPASSWD:ALL"]' >> $userdata
echo '    groups: wheel,adm,systemd-journal' >> $userdata
echo '    ssh_pwauth: True' >> $userdata
echo 'chpasswd:' >> $userdata
echo '  list: |' >> $userdata
echo '    root:cloud2018' >> $userdata
echo '    account:hon' >> $userdata
echo '  expire: False' >> $userdata

# Then create and label the VM disk:

virt-make-fs config_drive config-disk.qcow
e2label config-disk.qcow config-2

