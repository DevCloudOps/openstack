#!/bin/sh
echo yum all essential ocata packages
yum install chrony
yum install centos-release-openstack-ocata
yum install python-openstackclient
yum install openstack-selinux
yum install mariadb mariadb-server python2-PyMySQL
yum install rabbitmq-server
yum install memcached python-memcached
yum install openstack-keystone openstack-utils httpd mod_wsgi
yum install openstack-glance
yum install openstack-nova-api openstack-nova-conductor openstack-nova-console openstack-nova-novncproxy openstack-nova-scheduler openstack-nova-placement-api install openstack-nova-compute
yum install lvm2
yum install openstack-cinder targetcli python-keystone
yum install openstack-dashboard
yum install openstack-neutron openstack-neutron-ml2 openstack-neutron-linuxbridge openstack-neutron-openvswitch ebtables ipset

echo yum upgrade wants to install openvswitch 2.6.1 ... replacing our 2.7.2 ... no thanks



