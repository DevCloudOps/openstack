#!/bin/sh
# https://www.packtpub.com/mapt/book/virtualization_and_cloud/9781788294676/1/ch01lvl1sec11/preparing-images-for-os-installation-with-qemu-nbd
# https://manual.siduction.org/part-cfdisk
# https://stackoverflow.com/questions/26496470/convert-ext3-image-to-qcow2-format

#sz=10G
# after ext4 formating of primary partition, this should yield a 1.1 TB virtual drive:
sz=1128G
vdsk=ext4.qcow2
vdev=/dev/nbd0

function vmext4 {
  if [ $1 ] ; then sz="$1" ; fi
  if [ $2 ] ; then vdsk="$2" ; fi
  qemu-img create -f qcow2 $vdsk $sz
  modprobe nbd
  qemu-nbd -c $vdev $vdsk
  fdisk -l $vdev
# cfdisk $vdev
  fdisk $vdev
  sync
  mkfs.ext4 $vdev
  fdisk -l $vdev
  mkdir -p /mnt/$vdsk
  mount $vdev /mnt/$vdsk
  df -h /mnt/$vdsk
  umount /mnt/$vdsk
  qemu-nbd -d $vdev
}

fdisk -l | grep $vdev
fdisk -l | grep dev
df -h

