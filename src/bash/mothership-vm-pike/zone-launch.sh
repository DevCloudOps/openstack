https://docs.openstack.org/install-guide/launch-instance-provider.html
https://docs.openstack.org/nova/pike/admin/availability-zones.html

If your environment contains only one network, you can omit the --nic option because OpenStack automatically chooses the only network available.

Replace PROVIDER_NET_ID with the ID of the provider provider network.

openstack server create --image IMAGE --flavor m1.tiny --key-name KEY --availability-zone ZONE:HOST:NODE  --nic net-id=UUID SERVER

Or

openstack server create --flavor m1.nano --image cirros --nic net-id=PROVIDER_NET_ID --security-group default --key-name mykey provider-instance

