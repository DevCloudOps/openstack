#!/usr/bin/env python
"""
glance-pike.py:
Installed /etc/glance-api.conf file modifications for edployment.

https://www.sitepoint.com/12-favorite-atom-tips-and-shortcuts-to-improve-your-workflow/
"""

# http://python-future.org/quickstart.html ... writing code from scratch:
from __future__ import (absolute_import, division,print_function, unicode_literals)
#from builtins import *

import sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

import datetime, errno, getopt, json, logging, os, signal, socket, time
import copy, mmap, numpy, struct # pyinotify, zipfile
import ConfigParser

def filemmap(srcfile='foo', dstfile='backup'):
  """
  Use native mmap for fast efficient file duplication
  http://www.pythoncentral.io/memory-mapped-mmap-file-support-in-python/
  """
  dm = sm = sz = None
  try:
    sz = os.stat(srcfile).st_size ; sf = open(srcfile,'rb')
    os.mknod(dstfile); df = open(dstfile,'rw+') ; df.truncate(sz)
    sm = mmap.mmap(sf.fileno(), 0, access=mmap.ACCESS_READ)
    dm = mmap.mmap(df.fileno(), 0, access=mmap.ACCESS_WRITE)
    # not sure if dstfile should br re-sized  first?
    dm = copy.deepcopy(sm)
    sm.close() ; sf.close()
    dm.close() ; df.close()
  except:
    print('failed mmap of srcfile:', srcfile, dstfile, sz)

  return

define num_filemmap(srcfile='foo', dstfile='backup'):
  """
  Use numpy memap for fast efficient file duplication
  http://www.pythoncentral.io/memory-mapped-mmap-file-support-in-python/
  """
  dm = sm = sz = None
  try:
    sz = os.stat(srcfile).st_size ; sf = open(srcfile,'rb')
    os.mknod(dstfile); df = open(dstfile,'rw+') ; df.truncate(sz)
    sm = numpy.memap(sf.fileno(), 0, access=memap.ACCESS_READ)
    dm = numpy.memap(df.fileno(), 0, access=memap.ACCESS_WRITE)
    # not sure if dstfile should br re-sized  first?
    dm = copy.deepcopy(sm)
    sm.close() ; sf.close()
    dm.close() ; df.close()
  except:
    print('failed mmap of srcfile:', srcfile, dstfile, sz)

  return


if __name__ == '__main__':
  fastcopy()

