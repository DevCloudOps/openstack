#!/usr/bin/env python

"""
http://python-future.org/quickstart.html
https://cookiecutter.readthedocs.io/en/latest/
"""
from __future__ import (absolute_import, division, print_function, unicode_literals)
from builtins import *

import sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

import logging, os, string, time
from datetime import datetime

class ItunesDataCollectionApp(object):
  def __init__(self, *args, **kwargs):
    self.url = 'https://itunes.apple.com/search?term=beyonce&entity=musicVideo'
    if( args ): self.url = args[0] # simple assumption for starters
    self.today = basiclog.today()
    self.logpath = basiclog.conf(self.today+'itunes.log')
    logging.info('ItunesDataCollectionApp is Starting ... logging to: '+self.logpath)
    pass

  def fetch_data(self):
    """Returns a dict with current time and test data"""
    try:
      data = itunes.fetch(self.today, self.utl)
      return data
    except Exception:
      logging.debug(Exception)

  def current_time(self):
    """Returns the current time in UTC"""
    return datetime.utcnow()

  def fetch_interval(self, sec=10):
  # return itunes.fetchInterval(self.today, self.url, sec, 60*24)
    return None

def maintest():
  url = 'https://itunes.apple.com/search?term=beyonce&entity=musicVideo'
  interval = 10
  myapp = ItunesDataCollectionApp(url)
  myapp.fetch_interval(interval) # default is all day today

if __name__ == '__main__':
  maintest()

