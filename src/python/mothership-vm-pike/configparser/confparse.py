#!/usr/bin/env python
# http://python-future.org/quickstart.html ... writing code from scratch:
from __future__ import (absolute_import, division, print_function, unicode_literals)
# from builtins import *

import sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

from datetime import datetime
import copy, mmap
import ConfigParser

class SafeConParser(ConfigParser.SafeConfigParser):
  """
  https://pymotw.com/2/ConfigParser/
  Attributes of the object should include a list of found config file-names,
  and a list of those missing, as well as a confs dict {filename: config-obj}
  The ctor initially places {filename: None, ...} in the confs dict with the
  expectation that the methods loadFound and createMissing will be invoked later.
  From readthedocs and the examples:
  When adding sections or items, add them in the reverse order of how you want
  them to be displayed in the actual file. In addition, please note that using
  RawConfigParser's and the raw mode of ConfigParser's respective set functions,
  you can assign non-string values to keys internally, but will receive an error
  when attempting to write to a file or when you get it in non-raw mode.
  SafeConfigParser does not allow such assignments to take place.
  """
  def __init__(self, files=[], path='/var/tmp', log=None, **kwargs):
    if(len(files) <= 0): raise ValueError('Ctor of SafeConParser needs at least one config file name!')
    ConfigParser.SafeConfigParser.__init__(self)
    self.log = log
    pfiles = ['/'.join([path, files[i]]) for i in range(len(files))]
    found = self.read(pfiles)
    self.missing = sorted(set(pfiles) - set(found))
    self.found = sorted(set(found))
    self.confs = {} # this is a tad inefficient, can be passed thru method args ...
    for f in pfiles: self.confs[f] = None
    # an other args?
    try:
      for key, val in kwargs.items(): setattr(self, key, val)
    except: pass

    if( log ):
      self.log.info('found config files:', sorted(self.found))
      self.log.info('missing files     :', sorted(self.missing))

  def __str__(self):
    return repr(self.found)

  def __len__(self):
    return len(self.found) + len(self.missing)

  def loadFound(self, confs={}):
    """
    Load each config file found; presumaly the instance now has access to contents
    of all found config files. Note that all examples work with individual file configs,
    so this uses the confs dict { filename: confobj, ... }
    """
    for f in self.found: confs[f] = self.read(f)

    self.confs = copy.deepcopy(confs) # ineficient and not strictly necessary, but ...
    return len(self)

  def createMissing(self, confs={}, conitems=[{}]):
    """
    Write a non-DEFAULT section with at least 1 item into each newly created config file
    and read it back. Note that if one attempts to add_section['DEFAULT'] this exception
    is raised: ValueError, Invalid section name: DEFAULT ... presumably the DEFAULT section
    is always present.
    """
    if( len(conitems) <= 0 or len(conitems[0]) <= 0 ):
      conitems = [{ 'section': 'DEFAULT', 'key': 'Institution',
                    'val': 'University of Florida Interdisciplinary Center for Biotechnology Research (UF ICBR)' },
                  { 'section': 'UFICBR', 'key': 'Institution',
                    'val': 'University of Florida Interdisciplinary Center for Biotechnology Research (UF ICBR)' }]
    for f in self.missing:
      conf = ConfigParser.SafeConfigParser()
      for con in conitems:
        # print(con['section'], con['key'])
        if( 'DEFAULT' != con['section'] ): conf.add_section(con['section'])
        conf.set(con['section'], con['key'], con['val'])
      with open(f, 'wb') as fd: conf.write(fd)
      with open(f, 'r') as fd: confs[f] = conf.read(f)

    self.confs = copy.deepcopy(confs)
    return len(confs)

  @classmethod
  def writeall(cls):
    """
    Classmethod SafeConParser.writeall() writes all config attributes to their
    respective files.
    """
    for f,c in cls.confs.items():
      with open(f, 'wb') as fd: c.write(fd)

  @staticmethod
  def cpfile(srcfile='foo', dstfile='', path='/var/tmp'):
    """
    A utility funciton that uses mmap for fast efficient file duplication / backups
    http://www.pythoncentral.io/memory-mapped-mmap-file-support-in-python/
    """

    src = '/'.join([path,srcfile])
    dst = src + datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
    dm = sm = sz = None
    try:
      sz = os.stat(src).st_size ; sf = open(src,'rb')
      df = open(dst,'a+') ; df.truncate(sz) # os.mknod(dstfile) requires root on macos!
    except:
      print('failed mmap of:', src, dst, sz)
      return

    sm = mmap.mmap(sf.fileno(), 0, access=mmap.ACCESS_READ)
    dm = mmap.mmap(df.fileno(), 0, access=mmap.ACCESS_WRITE)
    dm[0:] = sm[0:]
    sm.close() ; sf.close() ; dm.close() ; df.close()
    return

  @staticmethod
  def backup(orig=['foo', 'bar'], path='/var/tmp'):
    for f in orig:
      SafeConParser.cpfile(f, path)

  @classmethod
  def backupall(cls, path='/var/tmp'):
    """
    Classmethod SafeConParser.backupall() writes all config attributes to their
    respective time-stamped backup files.
    """
    for f,c in cls.confs.items():
      SafeConParser.cpfile(f, path)

  @staticmethod
  def dbconnects(host='controller', pwd='cloud', dbcons={}):
    """
    Most of the Openstack subsystem configs. need Mariadb connection info.
    The default hostname of the Mariadb system is 'controller', which can be
    in the /etc/hosts or the local DNS. For example, the glance VM image service
    config file db entry should look like (glance db password set to 'cloud'):

    [database]
    connection = 'mysql+pymysql://glance:cloud@controller/glance'

    The above can be set with this API via this dict:
    { 'section': 'database', 'key': 'connection', 'val': 'mysql+pymysql://glance:cloud@controller/glance' }

    A typical Openstack deployment includes these services:
    ['cinder', 'dashboard', 'glance', 'keystone', 'magnum', 'neutron', 'nova']

    Returns size of db connection configs dict
    """

    dbc = { 'section': 'database', 'key': 'connection', 'val': None }
    subsys = ['cinder', 'dashboard', 'glance', 'keystone', 'magnum', 'neutron', 'nova']
    for s in subsys:
      dbc['val'] = 'mysql+pymysql://' + s + ':' + pwd + '@' + host + '/' + s # print(dbc)
      dbcons[sys] = copy.deepcopy(dbc)

    return len(dbcons)

  @staticmethod
  def version(header='SafeConPaser', footer='UF ICBR'):
    return header + 'version: 0.0.0 ' + footer

if __name__ == '__main__':
  try:
    con = SafeConParser()
  except: pass

  filelist = ['foo', 'bar']

  con = SafeConParser(filelist)
  print(con.version())
  con.backup(filelist)

  print(repr(con.confs))
  con.createMissing()
