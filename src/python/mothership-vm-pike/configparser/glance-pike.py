etcset#!/usr/bin/env python
"""
glance-pike.py:
Installed /etc/glance-api.conf file, modifications for edployment.

https://www.sitepoint.com/12-favorite-atom-tips-and-shortcuts-to-improve-your-workflow/
"""

# http://python-future.org/quickstart.html ... writing code from scratch:
from __future__ import (absolute_import, division,print_function, unicode_literals)
from builtins import *

import sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

import datetime, errno, getopt, json, logging, os, signal, socket, time
import copy, mmap # pyinotify, zipfile

# extension of ConfigParser.SafeConfigParser:
import confparse

_logger = None
_conflist = ['glance-api.conf', 'glance-cache.conf', 'glance-registry.conf', 'glance-swift.conf']

def initlog(logfile='pike-config.log'):
  """
  Log to stdout and to file.
  """
  global _logger
  _logger = logging.getLogger('pike-config-logger')
  _logger.addHandler(logging.FileHandler(logfile))
  _logger.addHandler(logging.StreamHandler(sys.stdout))
  _logger.info('setlog to stdout and to: ', logfile)
  return _logger

def openconfigs(path='/etc/pike/glance', files=_conflist):
  """
  Attempt to open and read given list of config files.
  Create any missing files with a simple default item or 2.
  Return dictionary of filename: config-object values.
  """
  global _logger
  if( not _logger ): initlog()

  config = None
  try:
    config = confparse.SafeConParser(files, path, _logger)
  except e:
    _logger.error('failed to read some or all config. files, so create an initial versions', files)

  if( len(config.missing) > 0 ):
    dbitem = { 'section': 'database', 'key': 'connection',
               'val': 'mysql+pymysql://glance:cloud@controller/glance'}
    defitem = { 'section': 'database', key: 'connection',
              'val': 'mysql+pymysql://glance:cloud@controller/glance'}
    itemlist = [dbitem, defitem]
    created = {}
    config.createMissing(created, itemlist)

  _logger.info('backup all original config files')
  config.backup(_conflist)

  # config object should no have a dictionay of all requested configs
  # key == config-file-name and val == SafeConfigParser instance
  return config.confs

def etcset(conf, conitem=_dbitem):
  """
  Insert config 'key = val' pairs into /etc/*.ini or .conf files
  If the etc file exists, first make a backup.
  Check if the indicated 'key = val' pair is present and if so replace it or
  comment it out and append new val; otherwise insert initial pair statement.
  # cp -p $apifile, $orig
  # crudini --set --existing $file, $section $key $val
  # crudini --set $file, $section $key $val
  # crudini --get $file, $section $key
  """
  key = val = None

  try:
    val = _config.get(dbitem['section'], dbitem['key'])
    _logger.info(val, item)
  except:
    _config.add_section(item['section'])

  _config.set(item.section, item.key, item.val)
  val = _config.get(item.section, item.key)
  _logger.info(_config.file, val, item)

  return config

def etcset_glance(apifile='glance-api.conf', regfile='glance-registry.conf']):
  datime = dateime.datetime()
  etcpath = '/etc/pike/glance/'
  config = openconfigs(etcpath, [apifile, regfile])

  apifile = ''.join(etcpath, apifile)
  backup = '-'.join(apifile, datime)
  cpfile(apifile, backup)

  regfile = ''.join(etcpath, regfile)
  backup = '-'.join(regfile, datime)
  cpfile(regfile, backup)

  etcset(apifile, 'database', 'connection', 'mysql+pymysql://glance:cloud@controller/glance')
  etcset(apifile, 'glance_store', 'stores', 'file,http')
  etcset(apifile, 'glance_store', 'default_store', 'file')
  etcset(apifile, 'glance_store', 'filesystem_store_datadir', '/var/lib/glance/images/')
  etcset(apifile, 'keystone_authtoken', 'auth_uri', 'http://controller:5000')
  etcset(apifile, 'keystone_authtoken', 'auth_url', 'http://controller:35357')
  etcset(apifile, 'keystone_authtoken', 'memcached_servers', 'controller:11211')
  etcset(apifile, 'keystone_authtoken', 'auth_type', 'password')
  etcset(apifile, 'keystone_authtoken', 'project_domain_name', 'default')
  etcset(apifile, 'keystone_authtoken', 'user_domain_name', 'default')
  etcset(apifile, 'keystone_authtoken', 'project_name', 'service')
  etcset(apifile, 'keystone_authtoken', 'username', 'glance')
  etcset(apifile, 'keystone_authtoken', 'password', 'cloud')
  etcset(apifile, 'paste_deploy', 'flavor', 'keystone')

  etcset(regfile, 'database', 'connection', 'mysql+pymysql://glance:cloud@controller/glance')
  etcset(regfile, 'keystone_authtoken', 'auth_uri', 'http://controller:5000')
  etcset(regfile, 'keystone_authtoken', 'auth_url', 'http://controller:35357')
  etcset(regfile, 'keystone_authtoken', 'memcached_servers', 'controller:11211')
  etcset(regfile, 'keystone_authtoken', 'auth_type', 'password')
  etcset(regfile, 'keystone_authtoken', 'project_domain_name', 'default')
  etcset(regfile, 'keystone_authtoken', 'user_domain_name', 'default')
  etcset(regfile, 'keystone_authtoken', 'project_name', 'service')
  etcset(regfile, 'keystone_authtoken', 'username', 'glance'
  etcset(regfile, 'keystone_authtoken', 'password', 'cloud')
  etcset(regfile, 'paste_deploy', 'flavor', 'keystone')

if __name__ == '__main__':
  etcset_glance()
