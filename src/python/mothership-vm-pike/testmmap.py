#!/usr/bin/env python
"""
http://www.pythoncentral.io/memory-mapped-mmap-file-support-in-python/
https://www.sitepoint.com/12-favorite-atom-tips-and-shortcuts-to-improve-your-workflow/
"""

# http://python-future.org/quickstart.html ... writing code from scratch:
from __future__ import (absolute_import, division,print_function, unicode_literals)
#from builtins import *

import sys
if sys.version_info.major < 3: import six # allows python3 syntax in python2

import datetime, errno, getopt, json, logging, os, signal, socket, time
import mmap # pyinotify, zipfile

def cpfile(srcfile='foo', dstfile='00'):
  """
  Use mmap for fast efficient file duplication
  http://www.pythoncentral.io/memory-mapped-mmap-file-support-in-python/
  """
  dstfile += srcfile
  dm = sm = sz = None
  try:
    sz = os.stat(srcfile).st_size ; sf = open(srcfile,'rb')
    df = open(dstfile,'a+') ; df.truncate(sz) # os.mknod(dstfile) requires root on macos!
  except:
    print('failed mmap of:', srcfile, dstfile, sz)
    return

  sm = mmap.mmap(sf.fileno(), 0, access=mmap.ACCESS_READ)
  dm = mmap.mmap(df.fileno(), 0, access=mmap.ACCESS_WRITE)
  dm[0:] = sm[0:]
  sm.close() ; sf.close() ; dm.close() ; df.close()
  return

if __name__ == '__main__':
  cpfile()
