self.fps_display = None
If it's a first-class attribute -- and reasonably visible -- it must be set to None
to release any resources. Deleting the attribute is creepy because the object now has
optional attributes and raises astonishing exceptions.

python -c 'from __future__ import print_function ;import gc ; print(gc.isenabled())'

