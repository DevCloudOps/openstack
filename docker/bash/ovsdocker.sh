#!/bin/sh
# http://cloudgeekz.com/400/how-to-use-openvswitch-with-docker.html
# http://www.securityandit.com/network/docker-kvm-network-integration/
# http://docker-k8s-lab.readthedocs.io/en/latest/docker/docker-ovs.html -- ovs veth pair with or without docker0 bridge

# http://blog.oddbit.com/2014/08/11/four-ways-to-connect-a-docker/
# This means that when your system comes back up, $eth will still be a member of ovs-cloudbr0,
# which will probably result in no network connectivity for your host.
# WARNING: The Open vSwitch configuration persists between reboots. 
# Before rebooting your system, make sure to ovs-vsctl del-port ovs-cloudbr0 $eth.

# https://kashyapc.fedorapeople.org/virt/openvswitch-and-libvirt-kvm.txt
# https://developer.ibm.com/recipes/tutorials/using-ovs-bridge-for-docker-networking/
# https://wiredcraft.com/blog/multi-host-docker-network/
# https://medium.com/@joatmon08/making-it-easier-docker-containers-on-open-vswitch-4ed757619af9
# http://www.tricksofthetrades.net/2016/01/27/docker-further-administration-networking/

systemctl stop NetworkManager.service
systemctl disable NetworkManager.service
systemctl enable network.service
systemctl start network.service

modprobe openvswitch
systemctl enable openvswitch.service
systemctl start openvswitch.service
systemctl status openvswitch.service

systemctl stop docker
ip link delete dev docker0

ovs-vsctl add-br ovs-cloudbr0
ip addr add dev ovs-cloudbr0 172.17.0.1/16
ip link set dev ovs-cloudbr0 up

ip link add veth_hostc0 type veth peer name veth_contc0
ovs-vsctl add-port ovs-cloudbr0 veth_hostc0

#ip route del default
#ip route add default via 172.17.0.1 dev ovs-cloudbr0

ovs-vsctl show

echo evidently this does not work -- bridge: ovs-cloudbr0 in /etc/docker/daemon.json
grep ovs-cloudbr0 /etc/docker/daemon.json
#if [ $? != 0 ] ; then
#  echo need bridge: ovs-cloudbr0 in /etc/docker/daemon.json
#  exit
#fi

systemctl start docker

docker network ls
journalctl -u docker | tail

