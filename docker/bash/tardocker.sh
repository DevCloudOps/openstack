#!/bin/bash

function describe {
  echo 0. check that docker runt-runtime exists, if so proceed with setting sig-handler and performing tasks:
  echo 1. get the container id
  echo 2. pause the container and commit a snapshot to a newly-named image
  echo 3. create a tarball from the image snapshot
  echo 4. describe how to restore and/or migrate via '(copy and)' load
  echo 5. interrupt signal handler should resume / unpause container on user-interrupt/abort of snaphot tar
}

_id=""
_name=""

function prompt {
  dflt='y'
  if [ $1 ] ; then dflt="$1" ; fi
  if [ ${BASH_VERSION%%.*} -ge 4 ] ; then
    read -e -p 'continue? [y/n]: ' -i $dflt ok
  else
    read -e -p 'continue? [y/n]: ' ok
    if [ "$ok" == "" ] ; then ok=${dflt} ; fi
  fi
  echo you entered: $ok
  if [[ $ok != y ]] ; then
    echo no worries ... aborting ...
    exit
  fi
}

function resume {
  which docker >& /dev/null
  if [ $? != 0 ] || [ $_name == "" ]; then
    echo sorry cannot cannot resume container $_name
    exit
  fi
  echo resuming / unpausing container $_i $_name
  docker unpause $_id
  if [ $? != 0 ] ; then
    echo sorry $_name container resume / unpause error? ... id indicates $_id
    exit 1
  fi
  exit 0
}

function tardocker {
  _name="tomcat8"
  if [ $1 ] ; then _name="$1" ; fi

  echo create tar snapshot of ${name}

  _id=`docker ps -a | grep "$name"`
  # CONTAINER ID        IMAGE                 COMMAND             CREATED             STATUS              PORTS                              NAMES
  # 1c4d70610526        hon/tomcat8-centos7   "tomcat.sh"         3 weeks ago         Up 3 weeks          8009/tcp, 0.0.0.0:8888->8080/tcp   tomcat8
  if [ $? != 0 ] ; then
    echo sorry ${_name} container not found? ... id indicates ${_id}
    return
  fi
  echo found ${_name} id == ${_id}

  prompt 'y'

  snap=${_name}-snapshot
  tarball=/var/tmp/${snap}.tar

  echo pausing container ${_id} and creating snapshot $tarball
  echo this can take awhile ... so if you wanna abort, hit control-c

  docker commit -p ${_id} ${snap}
  if [ $? != 0 ] ; then
    echo sorry ${_name} container pause and commit error? ... id indicates ${_id}
    return
  fi

  docker save -o $tarball $snap
  if [ $? != 0 ] ; then
    echo sorry ${_name} container tar-save error? ... id indicates ${_id}
    return
  fi

  echo to restore / boot this snapshot: docker load -i $tarball
}

# main

echo setting interrupt sig-handler ...
trap '{ echo "ok, you pressed Ctrl-C ... invoking exit handler ... " ; resume $_id $_name; }' INT

which docker >& /dev/null
if [ $? != 0 ] ; then
  echo sorry cannot find docker run-time ...
  prompt 'n'
fi

container='tomcat8'
if [ $1 ] ; then container="$1" ; fi
tardocker $container
