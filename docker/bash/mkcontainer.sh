#!/bin/sh
echo https://github.com/docker/for-linux/issues/106
echo https://developers.redhat.com/blog/2014/05/05/running-systemd-within-docker-container/

workarea=`pwd`
echo workarea == $workarea
allp=`cat packages`
supermin5 --verbose --use-installed --prepare -o ${workarea}/supermin.d $allp
# above cli creates packages file, but not hostfiles
\cp -p hostfiles ${workarea}/supermin.d

# the CLI above seems to work better ...
# feeding the package list via CLI yields a different outcome -- the contents of etc include passwd and shadow and other files?
# thus the openstack accounts are in place for use by each openstack subsys.

#\mkdir -p ${workarea}/supermin.d
#\cp -p packages hostfiles ${workarea}/supermin.d
#supermin5 --verbose --use-installed --prepare -o ${workarea}/supermin.d 

# populate appliance.d and create tarball from it 
supermin5 --verbose --build --format chroot -o ${workarea}/appliance.d ${workarea}/supermin.d

echo the hostfiles ensures top-level dirctories a created and some contents copied, but evidently not recursively
echo consequently must still rely on rsyncs:
pushd ${workarea}/appliance.d/var/lib
\rsync -va /var/lib/mysql .
popd

pushd ${workarea}/appliance.d/etc
\rsync -va /etc/pike ./
\rsync -va /etc/sysconfig/pike ./sysconfig
\rsync -va /etc/yum* .
#alletc='chrony.conf chrony.keys cinder glance haproxy heat httpd keystone logrotate.conf logrotate.d magnum my.cnf my.cnf.d neutron nova old-httpd openstack-dashboard openvswitch rabbitmq'
#for e in $alletc ; do mv $e .$e; ln -s pike/$e ; done
popd

pushd ${workarea}/appliance.d/usr/include
\rsync -va /usr/include/python2.7 .
popd

pushd ${workarea}/appliance.d/usr/lib
\rsync -va /usr/lib/python* .
popd

pushd ${workarea}/appliance.d/usr/lib64
\rsync -va /usr/lib64/python* .
popd

#echo ensure portability of mariadb data via a bind-mount of /open/openstack/mariad and configure accordingly
#echo 'datadir = /opt/openstack/mariadb' >> ./my.cnf.d/openstack.cnf

pushd $workarea/appliance.d

ls -al etc | grep pike

#tar -zcvf /var/tmp/docker-image.tar.gz .
#tar zcvf ../pike-varlib-feb2018.tar.gz /var/lib/{glance,heat,keystone,magnum,neutron,nova,openstack-dashboard,openvswitch,rabbitmq,selinux}

popd

