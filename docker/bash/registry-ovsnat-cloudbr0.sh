#!/bin/sh
# https://developer.ibm.com/recipes/tutorials/using-ovs-bridge-for-docker-networking/
# https://docs.docker.com/engine/reference/commandline/network_inspect/
# https://linuxconfig.org/how-to-retrieve-docker-container-s-internal-ip-address
# clean slate
. ./ovs-rmall.sh

# Add private IP to public NIC
host=`hostname -f`
pubIP=`grep $host /etc/hosts | awk '{print $1}'` # 63.141.239.90/29 == 90ghz.org
pubNIC=`ip addr|grep $pubIP|awk '{print $7}'` # enp5s0f1 # enp5s0f0

#pubovsIP=63.141.239.91 # another public IP available to the baremetal host free to assign to ovs bridge
pubovsIP=63.141.239.90 # another public IP available to the baremetal host free to assign to ovs bridge
priovsIP=172.17.0.1

# init the ovs bridge and give it a private and public IP:
ovs-vsctl add-br ovs-cloudbr0
ip addr add dev ovs-cloudbr0 ${pubovsIP}/29
ip addr add dev ovs-cloudbr0 ${priovsIP}/16
ip link set dev ovs-cloudbr0 up

ovs-vsctl show
ip addr show ovs-cloudbr0

# NAT mode
# configure an internal ip address on the ovs bridge
# creat two Docker Containers without a network
docker run -dit -p ${pubovsIP}:5000:5000 --name=registry --net=none registry

# connect containers to OVS bridge, assuming their respective NIC is eth0?
# does this create veth pairs from the ovs-cloudbr0 into the docker NICs (eth0)?
ovs-docker add-port ovs-cloudbr0 eth0 registry --ipaddress=172.17.0.10/16 --gateway=172.17.0.1
ovs-vsctl list-ports ovs-cloudbr0

# Add NAT and port-forwarding rules
# iptables -t nat -A POSTROUTING -o $pubNIC -j MASQUERADE
# iptables -A FORWARD -i ovs-cloudbr0 -j ACCEPT
# iptables -A FORWARD -i ovs-cloudbr0 -o $pubNIC -m state --state RELATED,ESTABLISHED -j ACCEPT

iptables-restore < registry_ovs-cloudbr0_iptables

iptables-save | tee /var/tmp/ovs-cloudbr0-iptables

docker ps

