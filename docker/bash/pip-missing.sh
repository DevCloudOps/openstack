#!/bin/sh
# yum reinstall python-devel
# systemd fails at compile dur to missing /usr/include/python2.7/Python.h
pip install --trusted-host pypi.python.org netaddr
pip install --trusted-host pypi.python.org requests
pip install --trusted-host pypi.python.org jinja2
python -c "import sys ; from glance.cmd.registry import main"

pip install --trusted-host pypi.python.org oslo_log
pip install --trusted-host pypi.python.org systemd


