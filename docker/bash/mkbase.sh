#!/bin/sh
# https://rwmj.wordpress.com/2014/03/08/supermin-version-5/

#workarea=/var/tmp/docker_bash ; mkdir -p $workarea
#supermin5 --verbose --prepare -o ${workarea}/supermin.d bash coreutils

#workarea=/var/tmp/docker_bash_python ; mkdir -p $workarea
#supermin5 --verbose --prepare -o ${workarea}/supermin.d bash coreutils python

#workarea=/var/tmp/docker_bash_yum ; mkdir -p $workarea
#supermin5 --verbose --prepare -o ${workarea}/supermin.d bash coreutils yum

#workarea=/var/tmp/docker_bash_php_python ; mkdir -p $workarea
#supermin5 --verbose --prepare -o ${workarea}/supermin.d bash coreutils php python

workarea=/var/tmp/docker_bash_python_tomcat ; mkdir -p $workarea
supermin5 --verbose --prepare -o ${workarea}/supermin.d bash coreutils curl python tomcat

#tar zcf ${workarea}/init.tar.gz ./init

#cat > ${workarea}/hostfiles <<EOF
#/usr/share/augeas/lenses/*.aug
#EOF

#cat > ${workarea}/excludefiles <<EOF
#-/usr/share/doc/*
#-/usr/share/info/*
#-/usr/share/man/*
#EOF

supermin5 --verbose --build --format chroot -o ${workarea}/appliance.d ${workarea}/supermin.d

pushd ${workarea}/appliance.d
tar --numeric-owner -czpf /var/tmp/docker_base.tar.gz  .
chown hon:users /var/tmp/docker_base.tar.gz

echo unlike a running image that hase been saved to a tarball and then loaded ...
echo 'the new supermin container image must be imported \(not loaded\) via:'
echo 'zcat /var/tmp/docker_base.tar | docker import - ${USER}/base'

echo start the container as a daemon:
echo docker run -dit --name ${USER}-base ${USER}/base /bin/bash

echo enter the container bash shell:
echo docker exec -it ${USER}-base bash

echo 'tag and push the image into a local registry (running on the local host listening on port 5000)'
echo docker tag icbr_base_bash_php_python localhost:5000/icbr_base_bash_php_python
echo docker push localhost:5000/icbr_base_bash_php_python 

echo verify the new image is available in the registry catalog
echo curl http://localhost:5000/v2/_catalog

