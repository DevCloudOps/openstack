from: https://www.sweharris.org/post/2017-07-16-dockerswarm/

% docker swarm init
% docker swarm join-token worker

There are two types of nodes in Swarm

Workers
A worker node is where the compute happens; when you tell Swarm to run 5 copies of your image then it will distribute the jobs across the workers.

Managers
A manager node is also a worker node, so can run jobs. But a manager node is also used to manage the swarm. You can (should!) have multiple managers, for resiliency. Now the managers communicate using the Raft consensus protocol. This means that over half of the managers need to agree on the state of the swarm. Typically that means you need an odd number of managers. With 3 managers you can suffer the loss of 1, and still keep running; with 5 managers you can lose 2; with 7 managers you can lose 3.. picking the right number of managers is an availability question and may depend on the underlying infrastructure “availability zones”.

Side note; managers can be made to not be workers by setting their worker availability to “drain” (docker node update --availability drain), which tells Swarm to remove any running jobs and don’t schedule new ones.

A worker can be promoted to a manager (docker node promote), and a manager can be demoted to a worker (docker node demote).

Docker Swarm works on the concepts of services. These define the unit to be deployed across the swarm. It can be as simple as a single container, or it could be a complicated setup similar to those described by docker-compose.

Let’s just start a copy of centos, and just set it pinging localhost:

% docker service create --detach=true --replicas 1 --name pinger centos ping localhost

% docker service ls
One container is boring. I want 3 of them!

% docker service scale pinger=3

The service can be stopped with docker service rm.
% docker service ps pinger

% docker node ls

If a server is planned to be taken down (e.g. for patching, rebooting) then the work load can be drained off of it first, and the node made quiescent:

% docker node update --availability drain docker-ce.spuddy.org

After maintenance is complete, the node can be made available again:

% docker node update --availability active docker-ce.spuddy.org
Docker Swarm has a built in load balancer. Let’s build a simple web server that has a CGI which reports on the hostname. I’m going to push this to docker hub with the name sweh/test. Once it’s pushed I can delete the local version (and the base image I built on)

% docker build -t test .

% docker tag test sweh/test

% docker push sweh/test

Let’s run 3 copies of this, exposing port 80. Notice I’m referring to the docker.io container name, so each engine can pull from the repo.
% docker service create --detach=true --replicas 3 --publish 80:80 --name httpd sweh/test
% docker service ps httpd
% curl localhost/cgi-bin/t.cgi
% curl test3/cgi-bin/t.cgi
% docker network inspect --format='{{ .IPAM.Config }}' ingress

This ingress network is the Swarm equivalent of docker0, but using the overlay driver across the swarm. If you look back at the example web server output, you’ll see the “Remote” address was a 10.255.0.x value; we’re seeing the load balancer at work on the ingress network.

Swarm can not run docker-compose directly. If you try it’ll warn you that you’re just running a single node service. Your compose file will work, but just on the node you started it on; there’s no Swarm interaction.

However, docker stack can read a compose file and create a stack of services out of it.

There are limitations, of course, due to the distributed nature of the run time. A big one, for example, is that filesystem based volumes don’t work so well (/my/dir on the manager may not be present on all the compute nodes!). There are work-arounds (different volume drivers; NFS;…) but this complexity is inherent in a multi-server model.

In earlier examples of docker-compose I created a 3-tier architecture (web,app,DB). Let’s see if we can do the same with Swarm. Now the DB is awkward; it needs access to specific files, so let’s run that on a fixed node.

% cat deploy.yaml
version: "3"

networks:
  webapp:
  appdb:

volumes:
  db-data:

services:
  web:
    image: sweh/test
    networks:
      - webapp
    ports:
      - 80:80

  app:
    image: centos
    networks:
      - webapp
      - appdb
    entrypoint: /bin/sh
    stdin_open: true
    tty: true

  db:
    image: mysql:5.5
    networks:
      - appdb
    environment:
      - MYSQL_ROOT_PASSWORD=foobar
      - MYSQL_DATABASE=mydb1
    volumes:
      - db-data:/var/lib/mysql
    deploy:
      placement:
        constraints: [node.hostname == test1.spuddy.org]
WARNING: This is a really bad setup. db-data is local to each node and so there’s no data persistency if the database is allowed to bounce around the swarm. It’s why we only allow it to run on test1. Do not do this for any production setup (use a better network aware volume setup!); I’m just showing it here for simplicity.

% docker stack deploy -c deploy.yaml 3tier

% docker stack ls

% docker stack services 3tier

% docker service ps 3tier

% ssh test1 docker network ls
% ssh test2 docker network ls | grep 3t

% ssh test3 docker network ls | grep 3t
inspect each service to see what IPs have been assigned:

% docker service inspect --format='{{ .Endpoint.VirtualIPs }}' 3tier_web

% docker service inspect --format='{{ .Endpoint.VirtualIPs }}' 3tier_app

% docker service inspect --format='{{ .Endpoint.VirtualIPs }}' 3tier_db

wondering if the docker instance start to have trouble in routing between the VXLAN and the primary network. So let’s shut this down (docker stack rm 3tier) and modify our stack definition to include network ranges:

networks:
  webapp:
    ipam:
      config:
        - subnet: 10.20.1.0/24
  appdb:
    ipam:
      config:
        - subnet: 10.20.2.0/24
Now when we run the stack we can see the services work as we expect and mirrors what we saw with docker-compose; the web layer can not see the db layer, but the app layer can see both.



