#!/bin/sh
# https://developer.ibm.com/recipes/tutorials/using-ovs-bridge-for-docker-networking/
# Create the OVS bridge
ovs-vsctl add-br ovs-cloudbr1

# NAT mode
# Configure an internal ip address on the ovs bridge
# Creat two ubuntu Docker Containers without network
docker run -d --name=container1 --net=none liguangcheng/ubuntu-16.04-ppc64el
docker run -d --name=container2 --net=none liguangcheng/ubuntu-16.04-ppc64el

#Connect the container to OVS bridge
ovs-docker add-port ovs-cloudbr1 $eth0 container1 --ipaddress=192.168.1.1/16 --gateway=192.168.0.1
ovs-docker add-port ovs-cloudbr1 $eth0 container2 --ipaddress=192.168.1.2/16 --gateway=192.168.0.1

# Add NAT rules
host=`hostname -f`
IP=`grep $host /etc/hosts | awk '{print $1}'`
#pubintf=`ifconfig | grep -B 1 $IP | head -1|cut -d':' -f1`
pubintf=`ip addr|grep $IP|awk '{print $7}'`
iptables -t nat -A POSTROUTING -o $pubintf -j MASQUERADE
iptables -A FORWARD -i ovs-cloudbr1 -j ACCEPT
iptables -A FORWARD -i ovs-cloudbr1 -o $pubintf -m state --state RELATED,ESTABLISHED -j ACCEPT

