#!/bin/sh
echo local registry:
docker run -dit -p 5000:5000 --name registry registry

echo 'portainer supports multiple (including local) registries'
docker run -d -p 9000:9000 --privileged --pid=host --name=portainer -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer

echo gitlab uses 22, 80, and 443
docker run -d -p 7022:22 -p 7080:80 -p 7443:443 --name=gitlab gitlab/gitlab-ce

echo do not assume atomichost 
\ls /ostree
if [ $? == 0 ] ; then
  docker run -d --privileged --pid=host --name=admin_toos -v /:/host centos/tools /container/atomic-run --local-ssh
  echo admin tools login via: 
  echo docker exec -it admin_tools bash
  echo cockpit uses tcp port 9090
  docker run -d --privileged --pid=host --name=cockpit -v /:/host cockpit/ws /container/atomic-run --local-ssh
fi

docker ps -a
