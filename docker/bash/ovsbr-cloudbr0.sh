#!/bin/sh
# https://developer.ibm.com/recipes/tutorials/using-ovs-bridge-for-docker-networking/
# https://docs.docker.com/engine/reference/commandline/network_inspect/
# https://linuxconfig.org/how-to-retrieve-docker-container-s-internal-ip-address
# clean slate
. ./ovs-rmall.sh

# Add private IP to public NIC
host=`hostname -f`
pubIP=`grep $host /etc/hosts | awk '{print $1}'` # 63.141.239.90/29 == 90ghz.org
pubNIC=`ip addr|grep $pubIP|awk '{print $7}'` # enp5s0f1 seems to be live and the primay access point

pubovsIP=63.141.239.91 # another public IP available to the baremetal host free to assign to ovs bridge
priovsIP=172.17.0.1

# init the ovs bridge and give it a private and public IP:
ovs-vsctl add-br ovs-cloudbr0
ip addr add dev ovs-cloudbr0 ${pubovsIP}/29
ip addr add dev ovs-cloudbr0 ${priovsIP}/16
ovs-vsctl add-port ovs-cloudbr0 enp5s0f0 # try to use NIC enp5s0f0 on ovs-cloudbr0
ip link set dev ovs-cloudbr0 up

ovs-vsctl show
ip addr show ovs-cloudbr0

# NAT mode
# configure an internal ip address on the ovs bridge
# creat two Docker Containers without a network
docker rm -f portainer >& /dev/null
docker rm -f gitlab >& /dev/null
docker run -dit -p ${pubovsIP}:5000:5000 --name=registry --net=none registry
docker run -dit -p ${pubovsIP}:9000:9000 --name=portainer --net=none portainer/portainer
docker run -dit -p ${pubovsIP}:9443:443 --name=gitlab --net=none gitlab/gitlab-ce

# connect containers to OVS bridge, assuming their respective NIC is eth0?
# does this create veth pairs from the ovs-cloudbr0 into the docker NICs (eth0)?
ovs-docker add-port ovs-cloudbr0 eth0 registry --ipaddress=172.17.0.10/16 --gateway=${priovsIP}
ovs-docker add-port ovs-cloudbr0 eth0 portainer --ipaddress=172.17.0.11/16 --gateway=${priovsIP}
ovs-docker add-port ovs-cloudbr0 eth0 gitlab --ipaddress=172.17.0.12/16 --gateway=${priovsIP}

ovs-vsctl list-ports ovs-cloudbr0

docker ps

