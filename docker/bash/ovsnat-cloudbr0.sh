#!/bin/sh
# https://developer.ibm.com/recipes/tutorials/using-ovs-bridge-for-docker-networking/
# https://docs.docker.com/engine/reference/commandline/network_inspect/
# https://linuxconfig.org/how-to-retrieve-docker-container-s-internal-ip-address
# clean slate
. ./ovs-rmall.sh

# Add private IP to public NIC
host=`hostname -f`
pubIP=`grep $host /etc/hosts | awk '{print $1}'` # 63.141.239.90/29 == 90ghz.org
pubNIC=`ip addr|grep $pubIP|awk '{print $7}'` # enp5s0f1 # enp5s0f0

pubovsIP=63.141.239.91 # assign public IP available to the baremetal host to ovs bridge
priovsIP=172.17.0.1 # also assign private IP to ovs bridge

# init the ovs bridge and give it a private and public IP:
ovs-vsctl add-br ovs-cloudbr0
ip addr add dev ovs-cloudbr0 ${pubovsIP}/29
ip addr add dev ovs-cloudbr0 ${priovsIP}/16
ip link set dev ovs-cloudbr0 up

# this shuts us out ... requieing rescue!
#ovs-vsctl add-port ovs-cloudbr0 $pubNIC

ovs-vsctl show
ip addr show ovs-cloudbr0

# NAT mode
# configure an internal ip address on the ovs bridge
# creat two Docker Containers without a network
docker rm -f registry >& /dev/null
docker rm -f portainer >& /dev/null
docker rm -f cockpit >& /dev/null
docker rm -f gitlab >& /dev/null

# gregory.fitness
docker run -dit -p ${pubIP}:8888:80 --name=fitness --net=none wger/apache

docker run -dit -p ${pubIP}:5000:5000 --name=registry --net=none registry
docker run -dit -p ${pubIP}:9000:9000 -v "/var/run/docker.sock:/var/run/docker.sock" --privileged --name=portainer --net=none portainer/portainer
docker run -dit -p ${pubIP}:9090:9090 --privileged --name=cockpit --net=none cockpit/ws
docker run -dit -p ${pubIP}:9022:22 -p ${pubIP}:9080:80 -p ${pubIP}:9443:443 --privileged --name=gitlab --net=none gitlab/gitlab-ce

# connect containers to OVS bridge, assuming their respective NIC is eth0?
# does this create veth pairs from the ovs-cloudbr0 into the docker NICs (eth0)?
fitnessIP=172.17.0.88 
registryIP=172.17.0.5 
portainerIP=172.17.0.9
cockpitIP=172.17.0.90
gitlabIP=172.17.0.80
gateway=172.17.0.1
echo $gateway $registryIP $portainerIP $cockpitIP $gitlabIP
ovs-docker add-port ovs-cloudbr0 eth0 fitness --ipaddress=${fitnessIP}/16 --gateway=${gateway}
ovs-docker add-port ovs-cloudbr0 eth0 registry --ipaddress=${registryIP}/16 --gateway=${gateway}
ovs-docker add-port ovs-cloudbr0 eth0 portainer --ipaddress=${portainerIP}/16 --gateway=${gateway}
ovs-docker add-port ovs-cloudbr0 eth0 gitlab --ipaddress=${gitlabIP}/16 --gateway=${gateway}
ovs-docker add-port ovs-cloudbr0 eth0 cockpit --ipaddress=${cockpitIP}/16 --gateway=${gateway}

ovs-vsctl list-ports ovs-cloudbr0

# Add NAT and port forwarding rules
iptables-restore < docker_ovs-cloudbr0_iptables_registry_portainer_cockpit_gitlab

docker ps
docker exec gitlab gitlab-ctl status
docker exec portainer ip addr
docker exec registry ip addr
curl 90ghz.org:5000/v2/_catalog

