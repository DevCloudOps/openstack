#!/bin/sh
echo local registry:
docker run -dit -p 5000:5000 --name registry registry

echo 'portainer supports multiple (including local) registries'
docker run -dit -p 9000:9000 --privileged --pid=host --name=portainer -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer

echo tomcat test
name=tomcat8
image=tomcat:8
docker run -dit -p 8080:8080 --name tomcat tomcat

echo gitlab uses 22, 80, and 443
docker run -dit -p 7022:22 -p 7080:80 -p 7443:443 --name=gitlab gitlab/gitlab-ce

echo do not assume atomichost 
\ls /ostree
if [ $? == 0 ] ; then
  echo this is atomichost so we could use the atomic CLI ...
  docker run -dit --privileged --pid=host --name=admin_tools -v /:/host centos/tools /container/atomic-run --local-ssh
  echo admin tools login via: 
  echo docker exec -it admin_tools bash
  echo cockpit uses tcp port 9090
  docker run -dit --privileged --pid=host --name=cockpit -v /:/host cockpit/ws /container/atomic-run --local-ssh
fi

docker ps -a

echo tag the active container and push it to local registry
docker tag $name localhost:5000/${image}
docker push localhost:5000/${image}


