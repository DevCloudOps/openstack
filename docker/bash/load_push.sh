#!/bin/sh
# https://blog.giantswarm.io/moving-docker-container-images-around/

tarfile=docker_base_bash.tar.gz

# load or import?
# gzcat $tarfile | docker load
gzcat $tarfile | docker import - icbr/base_bash

# no dns for local docker registry ... its in a localhost VM container port mapped 5000
docker tag icbr/base_bash localhost:5000/icbr/base_bash
docker push localhost:5000/icbr/base_bash
