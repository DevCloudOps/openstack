#!/bin/sh
# http://docs.openvswitch.org/en/latest/howto/docker/
host=`hostname -f`
lsmod | egrep 'geneve|stt'

export ENCAP_TYPE=geneve # stt
export LOCAL_IP=`grep $host /etc/hosts|awk '{print $1}'

/usr/share/openvswitch/scripts/ovn-ctl start_northd
ovn-nbctl set-connection ptcp:6641
ovn-sbctl set-connection ptcp:6642
ovs-vsctl set Open_vSwitch . \
    external_ids:ovn-remote="tcp:$CENTRAL_IP:6642" \
    external_ids:ovn-nb="tcp:$CENTRAL_IP:6641" \
    external_ids:ovn-encap-ip=$LOCAL_IP \
    external_ids:ovn-encap-type="$ENCAP_TYPE"
