#!/bin/sh

systemctl mask cloud-final.service
systemctl mask cloud-config.service

wkdir=config_drive/openstack/latest
mkdir -p $wkdir


metahost=${wkdir}/meta_data.json

userdata=${wkdir}/user_data


# Edit the 2 config files to contain:
echo '{ "uuid": "centos7generic" }' > $metahost


# and

echo '#cloud-config' > $userdata
echo 'users:' >> $userdata
echo '  - name: hon' >> $userdata
echo '    gecos: full user name for account' >> $userdata
echo '    sudo: ["ALL=(ALL) NOPASSWD:ALL"]' >> $userdata
echo '    groups: wheel,adm,systemd-journal' >> $userdata
echo '    ssh_pwauth: True' >> $userdata
echo 'chpasswd:' >> $userdata
echo '  list: |' >> $userdata
echo '    root:cloud2018' >> $userdata
echo '    account:hon' >> $userdata
echo '  expire: False' >> $userdata

# Then create and label the VM disk:

virt-make-fs config_drive config-disk.qcow2
e2label config-disk.qcow2 config-2

